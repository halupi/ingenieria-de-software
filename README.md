# Ingeniería de Software 2015-2 #

## Requerimientos ##
* Java 7 o posterior
* Play Framework
* PostgreSQL 9.3
* Git
* Sistema Operativo Windows / Mac / Linux
* Cualquier editor de textos

## Instalación del Proyecto ##

### Agregar play a las variables de entorno ###
Se necesita descargar y agregar el activator de play a las variables de entorno:

La descarga es desde https://www.playframework.com/download, y se siguen las instrucciones de https://www.playframework.com/documentation/2.0/Installing, 
que indican agregar el activator de play a las variables de entorno.

### Instalación de Java ###
* **Play** necesita tener Java en las variables de entorno, es decir, que aparezca lo siguiente ingresando los comandos **java** y **javac** en terminal:

``` 
#!bash
$ javac

Usage: javac <options> <source files>
where possible options include:
  -g                         Generate all debugging info
  -g:none                    Generate no debugging info
  -g:{lines,vars,source}     Generate only some debugging info
  -nowarn                    Generate no warnings
  -verbose                   Output messages about what the compiler is doing
  -deprecation               Output source locations where deprecated APIs are used
  -sourcepath <path>         Specify where to find input source files
  -bootclasspath <path>      Override location of bootstrap class files
  -extdirs <dirs>            Override location of installed extensions
  -endorseddirs <dirs>       Override location of endorsed standards path
  ...
```

```
#!bash
$ java

Usage: java [-options] class [args...]
           (to execute a class)
   or  java [-options] -jar jarfile [args...]
           (to execute a jar file)
where options include:
    -d32	  use a 32-bit data model if available
    -d64	  use a 64-bit data model if available
    -server	  to select the "server" VM
    -jamvm	  to select the "jamvm" VM
    -cacao	  to select the "cacao" VM
    -zero	  to select the "zero" VM
                  The default VM is server.
  ...
```

Dicha instalación varia según el S.O.

* Instalación en [Windows](http://www.aprenderaprogramar.com/index.php?option=com_content&id=389:configurar-java-en-windows-variables-de-entorno-javahome-y-path-cu00610b&Itemid=188)
* En Linux (distribuciones Debian) editar el archivo **.bashrc** ubicado en **/home/usuario** y escribir lo siguiente al final del archivo:
    * ``` export JAVA_HOME=/path/descomprimido/de/java ```
    * ``` export PATH=$PATH:$JAVA_HOME/bin ```
    * Cerrar la terminal, abrirla de nuevo y probar comandos **java** y **javac**.

* En OS X ya viene instalado por default. Verificar que sea Java >= 7 con el comando **java -version**

### Instalación de Git ###

Ahora toca instalar **Git** para descargar el repositorio, como lo dice [aquí](http://git-scm.com/downloads).

Una vez instalado, lo siguiente será descargar el repositorio con los siguientes comandos (aplican para Windows, Linux o Mac):

* Crear una carpeta con cualquier nombre, en donde ustedes quieran.
* Ingresar a la carpeta vía terminal ``` cd /path/carpeta ```
* ``` git init ``` crea lo archivos necesarios para usar comandos git sobre la carpeta
* ``` git remote add origin https://jorge-rmn@bitbucket.org/AQUI_VA_SU_USUARIO/ingenieria-de-software.git ``` ligará su carpeta con la principal del proyecto en BitBucket
* ``` git pull origin master ```  actualiza su carpeta a la del repositorio.

### Uso del sistema VGStore ###

* Clonar el respositorio
* Crear una base de datos llamada 'vgstore', con password 'halupisoft'
* Conectar a esa base de datos y ejecutar el script  /sql/vgstore.sql
* Para levantar el sistema, en la raíz del directorio donde se tiene clonado el repositorio desde terminal escribir:
    ``` activator run ```
     
 Despues de compilar e instalar los paquetes necesarios el sistema escucha en 
 ``` localhost:9000/ ```


 