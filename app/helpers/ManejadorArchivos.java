package helpers;

import play.mvc.Http;
import play.mvc.Http.MultipartFormData;
import play.mvc.Http.MultipartFormData.FilePart;

import java.io.File;
import java.util.List;
import java.util.LinkedList;
import java.io.IOException;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FileExistsException;

public class ManejadorArchivos {

	public ManejadorArchivos() {

	}

	/**
	 * Método que procesa el cuerpo de la petición y extrae los archivos, ubicandolos en el destino
	 * indicado por el usuario
	 * @param body El cuerpo de la petición
	 * @param nombres El valor del atributo name de cada input del formulario
	 * @param destino El path destino de los archivos.
	 * @return Una lista con los nombres de archivos procesados
	 * @throws IllegalArgumentException
	 */
	public List<String> procesarArchivos(Http.MultipartFormData body, String[] nombres, String destino) throws IllegalArgumentException {
		
		List<String> procesados = new LinkedList<String>();

		if( !validarParametros(body, nombres, destino) )
			throw new IllegalArgumentException();


		for( String nombre : nombres ){
			Http.MultipartFormData.FilePart data = body.getFile(nombre);
			if ( data != null ) {
				String nombreArchivo = data.getFilename();
				File archivo = data.getFile();
				try {
					FileUtils.moveFile(archivo, new File(destino, nombreArchivo));
					procesados.add(nombreArchivo);
				} catch(NullPointerException nullPointer) {
					continue;
				} catch(FileExistsException fileExists) {
					procesados.add(nombreArchivo);
				} catch(IOException io) {
					continue;
				}
			} else 
				continue;
		}

		return procesados;
	}

	/**
	 * Método para procesar un solo archivo
	 * @param body El cuerpo de la petición
	 * @param nombres El valor del atributo name de cada input del formulario
	 * @param destino El path destino de los archivos.
	 * @return El nombre del archivo procesado
	 * @throws IllegalArgumentException
	 */
	public String procesarArchivo(Http.MultipartFormData body, String nombre, String destino) throws IllegalArgumentException {
		List<String> procesados = procesarArchivos(body, new String[]{ nombre }, destino);
		return procesados.size() == 1 ? procesados.get(0) : null;
	}

	private boolean validarParametros(Http.MultipartFormData body, String[] nombres, String destino) {
		if( body == null || nombres == null || destino == null )
			return false;
		else if( nombres.length == 0 || destino.isEmpty() )
			return false;
		else
			return true;
	}

}