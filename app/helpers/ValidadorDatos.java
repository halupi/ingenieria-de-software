package helpers;

import com.fasterxml.jackson.databind.JsonNode;

import java.util.regex.Pattern;
import java.util.regex.Matcher;

public class ValidadorDatos {

	public ValidadorDatos() {

	}

	/**
	 * Método que valida que un correo sea válido
	 * @param sCorreo El texto a validar que debe ser un correo válido
	 * @return Un booleano que indica si el texto ingresado es un correo válido
	 * @throws IllegalArgumentException
	 */
	public Boolean validaCorreo(String sCorreo) throws IllegalArgumentException 
	{
		String sPatronCorreo;

		Pattern patron;
       	Matcher comparador;

		try
		{
			if(sCorreo == null || sCorreo.trim().isEmpty())
			{
				return false;
			}

			sPatronCorreo = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
            	+ "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

            patron = Pattern.compile(sPatronCorreo);
        	comparador = patron.matcher(sCorreo);

        	return comparador.matches();

        } catch(Exception oError){
        	return false;
        }
	}

	/**
	 * Método que valida si un JsonNode es nulo o contiene una cadena no vacía
	 * @param oData El JsonNode a revisar
	 * @param oNombresCampo Los nombres de los campos que se desean revisar, es un arreglo
	 * 						bidimensional que contiene el nombre de campo y un nombre para el mensaje
	 * @param sMensaje El mensaje a mostrar en el método que manda la validación
	 * @return True en caso de que el JsonNode sea nulo o contenga una cadena vacía
	 * @throws IllegalArgumentException
	 */
	public Boolean contieneNulosOVacios(JsonNode oData, String[][] oNombresCampo, String sMensaje) throws IllegalArgumentException 
	{
		Boolean bEstado;
		try
		{
			sMensaje = "";
			bEstado = false;

			for(String[] oCampo : oNombresCampo)
			{
				if(oData.get(oCampo[0]) == null || oData.get(oCampo[0]).asText().trim().isEmpty())
				{
					sMensaje += "\nEl campo de " + oCampo[1] + " es vacío o nulo.";
					bEstado = true;
				}
			}

        	return bEstado;

        } catch(Exception oError){
        	return false;
        }
	}

	/**
	 * Método que valida si una cadena es un entero
	 * @param sNumero La cadena que deseamos validar
	 * @return Un booleano que indica si la cadena es un valor entero
	 * @throws IllegalArgumentException
	 */
	public Boolean esInteger(String sNumero) throws IllegalArgumentException 
	{
		try
		{
			Integer.parseInt(sNumero);
			return true;

		} catch(NumberFormatException oException){
			return false;
		}
	}

	/**
	 * Método que valida si una cadena es un dobule
	 * @param sNumero La cadena que deseamos validar
	 * @return Un booleano que indica si la cadena es un valor double
	 * @throws IllegalArgumentException
	 */
	public Boolean esDouble(String sNumero) throws IllegalArgumentException 
	{
		try
		{
			Double.parseDouble(sNumero);
			return true;

		} catch(NumberFormatException oException){
			return false;
		}
	}
}