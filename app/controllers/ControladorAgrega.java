package controllers;

import play.*;
import play.mvc.*;

import models.*;
import models.base.*;
import views.html.*;
import helpers.ManejadorArchivos;

import play.libs.Json;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;

import java.io.File;
import java.io.IOException;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FileExistsException;

import java.util.List;
import java.util.LinkedList;
import java.util.Iterator;


public class ControladorAgrega extends Controller {

	public static Result subirArchivo() {

		Http.MultipartFormData multipart = request().body().asMultipartFormData();
		ManejadorArchivos manejador = new ManejadorArchivos();
		ObjectNode respuesta = Json.newObject();

		try {
			String archivo = manejador.procesarArchivo(multipart, "file", "public/images/games");
			if( archivo == null ) {
				respuesta.put("error", true);
				respuesta.put("mensaje", "No se pudo procesar el archivo2");
			} else 
				respuesta.put("archivo", archivo);
		} catch(IllegalArgumentException ex) {
			respuesta.put("error", true);
			respuesta.put("mensaje", "Ocurrio un error mientras se procesaba el archivo");
		}

		return ok(respuesta);
	}

	public static Result subirJuego() {

		Http.MultipartFormData multipart = request().body().asMultipartFormData();
		ManejadorArchivos manejador = new ManejadorArchivos();
		ObjectNode respuesta = Json.newObject();

		try {
			String archivo = manejador.procesarArchivo(multipart, "file", "games");
			if( archivo == null ) {
				respuesta.put("error", true);
				respuesta.put("mensaje", "No se pudo procesar el archivo");
			} else 
				respuesta.put("archivo", archivo);
		} catch(IllegalArgumentException ex) {
			respuesta.put("error", true);
			respuesta.put("mensaje", "Ocurrio un error mientras se procesaba el archivo");
		}

		return ok(respuesta);
	}

	@BodyParser.Of(BodyParser.Json.class)
	public static Result guardaJuego() {
		JsonNode data = request().body().asJson();
		ObjectNode respuesta = Json.newObject();

		if( data == null) {
			respuesta.put("error", true);
			respuesta.put("mensaje", "Ocurrio un error al guardar el juego");
			return ok(respuesta);
		}

		int idJuego = data.get("id") != null ? data.get("id").asInt() : 0;
		int idUsuario = data.get("usuario_id") != null ? data.get("usuario_id").asInt() : 0;

		if( idJuego == 0)
			idUsuario = Integer.parseInt( session("usuario") );

		Videojuego nuevo = new Videojuego(
			idJuego,
			idUsuario,
			data.get("nombre").asText(),
			data.get("descripcion").asText(),
			data.get("precio").asInt(),
			data.get("video").asText(),
			data.get("publicacion") != null ? data.get("publicacion").asText() : ""
		);

		String juego = data.get("archivo").asText();

		nuevo.asignarJuego( juego );
		nuevo.cargarImagenes();

		Iterator<JsonNode> iterador = data.get("imagenes").elements();
		List<String> imagenes = new LinkedList<String>();

		while( iterador.hasNext() ) {
			String sImagen = iterador.next().asText();
			if(!nuevo.obtenerImagenes().contains(sImagen)) { 
				imagenes.add(sImagen);
			}
		}

		iterador = data.get("imagenes_borradas").elements();
		List<String> imagenesPorBorrar = new LinkedList<String>();

		while( iterador.hasNext() ) {
			String sImagen = iterador.next().asText();
			System.out.println("imagen a borrar " + sImagen);
			if(nuevo.obtenerImagenes().contains(sImagen)) { 
				imagenesPorBorrar.add(sImagen);
			}
		}

		int id = 0;

		ModeloAgrega modelo = new ModeloAgrega();
		if(nuevo != null){
			if(idJuego > 0)
				id = modelo.actualizaJuego(nuevo);
			else 
				id = modelo.guardaJuego(nuevo);
		}

		if(imagenes.size() > 0)
			modelo.guardaImagenes(imagenes, id);

		if(imagenesPorBorrar.size() > 0)
			modelo.borrarImagenes(imagenesPorBorrar, id);

		respuesta.put("id", id);

		return ok(respuesta);
	}

	public static Result editarJuego(Integer id) {
		ObjectNode respuesta = Json.newObject();
		
		if( id == 0) {
			respuesta.put("error", true);
			respuesta.put("mensaje", "No se encuentra el juego especificado.");
			return ok( respuesta );
		}


		ModeloAgrega agrega = new ModeloAgrega();
		Videojuego juego = Videojuego.obtenerPorId(id);

		if( juego == null ) {
			respuesta.put("error", true);
			respuesta.put("mensaje", "No se encuentra el juego especificado.");
		} else {
			juego.cargarImagenes();
			respuesta = juego.toJSON();
		}

		return ok( respuesta );
	}	
}