package controllers;

import play.*;
import play.mvc.*;
import models.base.Usuario;
import views.html.partials.*;

public class ControladorParciales extends Controller {

	public static Result registro() {
		String usuario_id = session("usuario");
		return ok( usuario_id == null ? registro.render() : denegado.render() );
	}

	public static Result actualizar() {
		String usuario_id = session("usuario");
		return ok( usuario_id != null ? registro.render() : denegado.render() );
	}

	public static Result juegos() {
		return ok( juegos.render() );
	}

	public static Result agregar() {
		String usuario_id = session("usuario");
		return ok( Usuario.esAdmin(usuario_id) ? agregar.render() : denegado.render() );
	}

	public static Result listado() {
		String usuario_id = session("usuario");
		return ok( Usuario.esAdmin(usuario_id) ? listado.render() : denegado.render() );
	}

	public static Result admin() {
		String usuario_id = session("usuario");
		return ok( Usuario.esAdmin(usuario_id) ? admin.render() : denegado.render() );
	}
	
	public static Result pendientes(){
		String usuario_id = session("usuario");
		return ok( Usuario.esAdmin(usuario_id) ? pendientes.render() : denegado.render() );
	}

	public static Result error() {
		return ok( error.render() );
	}

	public static Result cuenta() {
		String usuario_id = session("usuario");
		return ok( usuario_id != null ? cuenta.render() : denegado.render() );
	}
}