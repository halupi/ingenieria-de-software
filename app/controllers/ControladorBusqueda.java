package controllers;

import play.*;
import play.mvc.*;

import models.*;
import models.base.*;
import views.html.*;

import play.libs.Json;
import java.util.List;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.fasterxml.jackson.databind.node.ObjectNode;

public class ControladorBusqueda extends Controller {

	public static Result buscar(String query) {
		if( query == null )
			return ok(Json.newObject());
		
		List<Videojuego> resultado = Videojuego.busqueda(query);
		ArrayNode arreglo = new ArrayNode( JsonNodeFactory.instance );

		for( Videojuego juego : resultado ) {
			juego.cargarImagenes();
			arreglo.add( juego.toJSON() );
		}

		return ok(arreglo);
	}
	
}