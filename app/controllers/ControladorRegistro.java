package controllers;

import play.*;
import play.mvc.*;

import models.*;
import models.base.*;
import views.html.*;
import helpers.*;

import play.libs.Json;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;

import java.io.File;
import java.io.IOException;
import java.util.regex.Pattern;
import java.util.regex.Matcher;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FileExistsException;

public class ControladorRegistro extends Controller {

	public static Result registro(Integer id) {
		ObjectNode respuesta = Json.newObject();
		
		if( id == 0) {
			respuesta.put("error", true);
			respuesta.put("mensaje", "No se encuentra el usuario especificado.");
			return ok( respuesta );
		}

		Usuario usuario = Usuario.obtenerPorId(id);

		if( usuario == null ) {
			respuesta.put("error", true);
			respuesta.put("mensaje", "No se encuentra el usuario especificado.");
		} else {
			respuesta = usuario.toJSON();
		}

		return ok( respuesta );
	}

	@BodyParser.Of(BodyParser.Json.class)
	public static Result guardaUsuario() {
		String sMensaje = "", sApellidoMaterno = "";		
		
		ValidadorDatos oValidador = new ValidadorDatos();

		JsonNode data = request().body().asJson();
		ObjectNode respuesta = Json.newObject();

		String[][] sCampos = new String[][]{{"username", "Nombre de usuario"}, {"password", "Contraseña"},
									{"nombre", "Nombre del estudiante"}, {"paterno", "Apellido paterno"},
									{"correo", "Correo electrónico"},{"edad", "Edad"}, {"escuela", "Escuela"},
									{"promedio", "Promedio"}, {"historial", "Historial"}};

		if(oValidador.contieneNulosOVacios(data, sCampos, sMensaje))
		{
			respuesta.put("error", true);
    		respuesta.put("mensaje", sMensaje);
    		return ok(respuesta);
		}

		if(data.get("edad") != null && !oValidador.esInteger(data.get("edad").asText()))
		{
			sMensaje = "\nEl campo edad no es numérico";
			respuesta.put("error", true);
    		respuesta.put("mensaje", sMensaje);
    		return ok(respuesta);
		}

		if(data.get("promedio") != null && !oValidador.esDouble(data.get("promedio").asText()))
		{
			sMensaje = "\nEl campo promedio no es numérico";
			respuesta.put("error", true);
    		respuesta.put("mensaje", sMensaje);
    		return ok(respuesta);
		}

		//Validando correo electrónico
		if(data.get("correo") != null && !oValidador.validaCorreo(data.get("correo").asText()))
		{
			sMensaje = "\nEl correo no es válido";
			respuesta.put("error", true);
    		respuesta.put("mensaje", sMensaje);
    		return ok(respuesta);
    	}	

    	if(data.get("materno") == null || data.get("materno").asText().trim().isEmpty())
		{
			sApellidoMaterno = "";
		} 
		else 
		{
			sApellidoMaterno = data.get("materno").asText();
		}

		int id = data.get("id") != null ? data.get("id").asInt() : 0;

		Usuario nuevo = new Usuario(
			id,
			data.get("username").asText(),
			data.get("password").asText(),
			data.get("nombre").asText(),
			data.get("paterno").asText(),
			sApellidoMaterno,
			data.get("correo").asText(),
			data.get("edad").asInt(),
			data.get("escuela").asText(),
			data.get("promedio").asDouble(),
			data.get("historial").asText()
		);

		ModeloRegistro registro = new ModeloRegistro();

		try {
			if( id == 0)
				id = registro.guardaUsuario(nuevo);
			else
				registro.actualizaUsuario(nuevo);
			respuesta.put("id", id);
		} catch(Exception e) {
			respuesta.put("error", true);
			if( e.getMessage().indexOf("usuario_username_key") != -1 )
				respuesta.put("mensaje", "El nombre de usuario ya existe.");
			else if( e.getMessage().indexOf("usuario_datos_mail_key") != -1 )
				respuesta.put("mensaje", "El correo electrónico ya fue registrado por otro usuario.");
			else
				respuesta.put("mensaje", "Ocurrió un error inesperado");
		}
		return ok(respuesta);
	}

	public static Result subirArchivo() {

		Http.MultipartFormData multipart = request().body().asMultipartFormData();
		ManejadorArchivos manejador = new ManejadorArchivos();
		ObjectNode respuesta = Json.newObject();

		try {
			String archivo = manejador.procesarArchivo(multipart, "file", "docs/historiales");
			if( archivo == null ) {
				respuesta.put("error", true);
				respuesta.put("mensaje", "No se pudo procesar el archivo");
			} else 
				respuesta.put("archivo", archivo);
		} catch(IllegalArgumentException ex) {
			respuesta.put("error", true);
			respuesta.put("mensaje", "Ocurrio un error mientras se procesaba el archivo");
		}

		return ok(respuesta);
	}

}