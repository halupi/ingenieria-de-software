package controllers;

import play.*;
import play.mvc.*;

import models.*;
import models.base.*;
import views.html.*;

import play.libs.Json;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;

public class ControladorLogin extends Controller {

	public static Result logout() {
		session().clear();
		return redirect("/#/juegos");
	}

	public static Result login() {
		JsonNode data = request().body().asJson();
		ObjectNode respuesta = Json.newObject();

		String username = data.get("username").asText();
		String password = data.get("password").asText();

		ModeloLogin login = new ModeloLogin();
		Usuario usuario;
		int usuario_id = login.obtenerUsuario(username, password);
		int creditos = 0;

		if( usuario_id != 0 ) {
			session( "usuario", Integer.toString(usuario_id) );
			usuario = Usuario.obtenerPorId( usuario_id );
			usuario.cargarCreditos();
			respuesta = usuario.toJSON();

			if( Usuario.esAdmin(usuario_id) )
				respuesta.put("es_admin", true);
		} else {
			respuesta.put("error", true);
			respuesta.put("mensaje", "El usuario o contraseña no coinciden.");
		}

		return ok(respuesta);
	}
	
}