package controllers;

import play.*;
import play.mvc.*;

import models.*;
import models.base.*;
import views.html.*;

import play.libs.Json;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.fasterxml.jackson.databind.node.ArrayNode;
import java.util.List;

public class ControladorInicio extends Controller {

	public static Result index() {
		return ok( index.render() );
	}

	public static Result sesion() {
		String usuario_id = session("usuario");
		ObjectNode respuesta = Json.newObject();
		Usuario usuario;

		if( usuario_id == null )
			respuesta.put("error", true);
		else {
			usuario = Usuario.obtenerPorId( Integer.parseInt(usuario_id) );

			if( usuario == null )
				respuesta.put("error", true);
			else {
				usuario.cargarCreditos();
				respuesta = usuario.toJSON();
				respuesta.put("logged_in", true);
			}

			if( Usuario.esAdmin(Integer.parseInt(usuario_id)) )
				respuesta.put("es_admin", true);
		}
		return ok(respuesta);
	}

	public static Result comprados() {
		ArrayNode arreglo = new ArrayNode( JsonNodeFactory.instance );
		String usuario_id = session("usuario");

		if( usuario_id == null )
			return ok( arreglo );

		ModeloJuego modelo = new ModeloJuego();
		List<Videojuego> juegos = modelo.comprados( Integer.parseInt(usuario_id) );

		for( Videojuego juego : juegos )
			arreglo.add( juego.toJSON() );

		return ok( arreglo );
	}
	
}	