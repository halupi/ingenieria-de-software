package controllers;

import play.*;
import play.mvc.*;

import models.*;
import models.base.*;
import views.html.*;
import helpers.ManejadorArchivos;

import play.libs.Json;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.fasterxml.jackson.databind.node.ArrayNode;

import java.io.File;
import java.io.IOException;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FileExistsException;
import java.util.List;

public class ControladorPendientes extends Controller{
	public static Result pendientes(){
		ModeloPendientes modelo=new ModeloPendientes();
		List<Pendiente> pendientes=modelo.buscaPendientes();
		ArrayNode arreglo=new ArrayNode(JsonNodeFactory.instance);
		for(Pendiente pendiente : pendientes){
			arreglo.add(pendiente.toJSON());
		}
		return ok(arreglo);
	}
    @BodyParser.Of(BodyParser.Json.class)		
	public static Result aprueba(){
		JsonNode data = request().body().asJson();
		ObjectNode respuesta = Json.newObject();
		ModeloPendientes modelo = new ModeloPendientes();

		if( data == null || data.get("id") == null || data.get("credito") == null ) {
			respuesta.put("error", true);
			respuesta.put("mensaje", "Ocurrió un error al aprobar, intente de nuevo.");
			return ok( respuesta );
		}

		modelo.aprueba( data.get("id").asInt(), data.get("credito").asInt() );
		return ok();
	}
	@BodyParser.Of(BodyParser.Json.class)		
	public static Result denegar(){
		JsonNode data = request().body().asJson();
		ObjectNode respuesta = Json.newObject();
		ModeloPendientes modelo=new ModeloPendientes();

		if( data == null || data.get("id") == null ) {
			respuesta.put("error", true);
			respuesta.put("mensaje", "Ocurrió un error al aprobar, intente de nuevo.");
			return ok( respuesta );
		}

		modelo.denegar( data.get("id").asInt() );
		return ok();
	}
}