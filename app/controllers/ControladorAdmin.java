package controllers;

import play.*;
import play.mvc.*;

import models.*;
import models.base.*;
import views.html.*;

import play.libs.Json;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;

import java.io.File;

public class ControladorAdmin extends Controller {
	public static Result descargarHistorial(int id) {
		String usuario_id = session("usuario");

		if( !Usuario.esAdmin(usuario_id) )
			return redirect("/#/404");

		String path = Play.application().path().getAbsolutePath();

		Usuario usuario = Usuario.obtenerPorId( id );

		if( usuario == null )
			return redirect("/#/404");

		File archivo = new File(path + "/docs/historiales/" + usuario.obtenerHistorial());

		if( !archivo.exists() || archivo.isDirectory() )
			return redirect("/#/404");

		return ok( archivo );
	}
}