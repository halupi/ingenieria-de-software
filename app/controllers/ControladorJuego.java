package controllers;

import play.*;
import play.mvc.*;

import models.*;
import models.base.*;

import play.libs.Json;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.fasterxml.jackson.databind.node.ArrayNode;

import java.io.File;
import java.io.IOException;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FileExistsException;
import java.util.List;

public class ControladorJuego extends Controller {

	public static Result todos(int p) {
		System.out.println(p);
		List<Videojuego> juegos = Videojuego.catalogoCompleto(p);

		ArrayNode arreglo = new ArrayNode( JsonNodeFactory.instance );

		for( Videojuego juego : juegos ) {
			juego.cargarImagenes();
			arreglo.add( juego.toJSON() );
		}

		return ok( arreglo );
	}

	public static Result vendidos(int p) {
		ArrayNode arreglo = new ArrayNode( JsonNodeFactory.instance );

		List<Videojuego> juegos = Videojuego.catalogoVendidos(p);

		for( Videojuego juego : juegos ) {
			juego.cargarImagenes();
			arreglo.add( juego.toJSON() );
		}

		return ok( arreglo );
	}

	public static Result recientes(int p) {
		ArrayNode arreglo = new ArrayNode( JsonNodeFactory.instance );

		List<Videojuego> juegos = Videojuego.catalogoRecientes(p);

		for( Videojuego juego : juegos ) {
			juego.cargarImagenes();
			arreglo.add( juego.toJSON() );
		}

		return ok( arreglo );
	}

	public static Result gratuitos(int p) {
		ArrayNode arreglo = new ArrayNode( JsonNodeFactory.instance );

		List<Videojuego> juegos = Videojuego.catalogoGratuitos(p);

		for( Videojuego juego : juegos ) {
			juego.cargarImagenes();
			arreglo.add( juego.toJSON() );
		}

		return ok( arreglo );
	}

	public static Result descarga(int usuario_id, int videojuego_id) {
		String path = Play.application().path().getAbsolutePath();

		String usuario_sesion = session("usuario");

		if( (usuario_sesion == null && usuario_id > 0) || (usuario_sesion != null && Integer.parseInt(usuario_sesion) != usuario_id) )
			return redirect("/#/404");

		Videojuego juego = Videojuego.obtenerPorId( videojuego_id );

		if( juego.obtenerPrecio() > 0 )
			juego = Videojuego.obtenerComprado( videojuego_id, usuario_id );

		if( juego == null )
			return redirect("/#/404");

		if( usuario_id == 0 && juego.obtenerPrecio() > 0 )
			return redirect("/#/404");

		File archivo = new File(path + "/games/" + juego.obtenerJuego());

		if( !archivo.exists() || archivo.isDirectory() )
			return redirect("/#/404");

		return ok( archivo );
	}

	public static Result comprar() {
		JsonNode data = request().body().asJson();
		ObjectNode respuesta = Json.newObject();

		if( data == null ) {
			respuesta.put("error", true);
			respuesta.put("mensaje", "Ocurrio un error inesperado al procesar la compra.");
			return ok( respuesta );
		}

		if( data.get("videojuego_id") == null ) {
			respuesta.put("error", true);
			respuesta.put("mensaje", "Debe seleccionar un juego para comprar.");
			return ok( respuesta );
		}

		int videojuego_id = data.get("videojuego_id").asInt();

		String usuario_id = session("usuario");

		if( usuario_id == null ) {
			respuesta.put("error", true);
			respuesta.put("mensaje", "Debes iniciar sesión para poder comprar el juego");
			return ok( respuesta );
		}

		Usuario usuario = Usuario.obtenerPorId( Integer.parseInt(usuario_id) );
		usuario.cargarCreditos();

		Videojuego juego = Videojuego.obtenerPorId( videojuego_id );

		if( usuario.obtenerCreditos() < juego.obtenerPrecio() ) {
			respuesta.put("error", true);
			respuesta.put("mensaje", "No tienes los créditos suficientes para poder comprarlo");
			return ok( respuesta );
		}

		ModeloJuego modelo = new ModeloJuego();
		modelo.comprar( Integer.parseInt(usuario_id), videojuego_id, juego.obtenerPrecio() );

		respuesta.put("ok", true);
		respuesta.put("descontado", juego.obtenerPrecio());
		respuesta.put("mensaje", "Tu compra se ha realizado con éxito, gracias.");
		return ok( respuesta );
	}
	
}