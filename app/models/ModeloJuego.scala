package models

import anorm._
import anorm.SqlParser._
import play.api.db.DB
import play.api.Play.current
import models.base._
import scala.collection.JavaConverters._
import java.util.Date

class ModeloJuego {

	def comprar( usuario_id: Int, videojuego_id: Int, precio: Int ): Unit = {
      	DB withConnection { implicit c =>
			SQL(
				"""
					INSERT INTO compra VALUES ({usuario_id}, {videojuego_id}, NOW())
				"""
			)
			.on(
				"usuario_id" -> usuario_id,
				"videojuego_id" -> videojuego_id
			)
			.executeInsert()
		}

		if( precio > 0 ) {
			DB withConnection { implicit c =>
				SQL(
					"""
						UPDATE estudiante SET credito = credito - {cantidad} WHERE usuario_id = {usuario_id}
					"""
				)
				.on(
					"usuario_id" -> usuario_id,
					"cantidad" -> precio
				)
				.executeUpdate()
			}
		}
   }

   def comprados( usuario_id: Int ): java.util.List[Videojuego] = {
   		var juegos: List[Videojuego] = null;

   		DB withConnection { implicit c =>
			val sql = SQL(
				"""
					SELECT *
					FROM
						videojuego JOIN compra ON id = videojuego_id
					WHERE
						compra.usuario_id = {usuario_id}
				"""
			)
			.on( "usuario_id" -> usuario_id )

			juegos = sql().map( row => new Videojuego(
					row[Int]("id"),
					row[Int]("usuario_id"),
					row[String]("nombre"),
					row[String]("descripcion"),
					row[Int]("precio"),
					row[String]("video"),
					row[Date]("publicacion").toString()
				)
			).toList
		}

		return juegos.asJava;
   }

}