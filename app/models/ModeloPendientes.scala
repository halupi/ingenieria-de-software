package models

import anorm._
import anorm.SqlParser._
import play.api.db.DB
import play.api.Play.current
import models.base._
import scala.collection.JavaConverters._
import scala.collection.mutable.ListBuffer
class ModeloPendientes{
	def buscaPendientes():java.util.List[Pendiente]={
		var pendientes:List[Pendiente]=null;

		var sql="select * from (select * from usuario inner join (select * from usuario_datos where usuario_id in (select usuario_id from historial where status_id=3)) as foo on (usuario.id=usuario_id)) as fooo inner join (select usuario_id, url from historial) as hist on (hist.usuario_id=fooo.usuario_id);"
			
		DB withConnection { implicit c =>
			val pendientesSQL = SQL( sql )
			
			pendientes = pendientesSQL().map(row => new Pendiente(
				row[Int]("usuario_id"),
				row[String]("username"),
				row[String]("nombre"),
				row[String]("apellido_paterno"),
				row[String]("apellido_materno"),
				row[String]("mail"),
				row[Int]("edad"),
				row[String]("escuela"),
				row[Double]("promedio"),
				row[String]("url"),
				0
			)).toList
		}
		return pendientes.asJava
	}
	def aprueba(usuario_id: Int, credito: Int): Unit={
		
		DB withConnection { implicit c =>
			try {
				var s0=SQL(
					"""
						INSERT INTO 
							estudiante
						VALUES 
							(
								{usuarioId}, 
								{credito}
							)
					"""
				)
				.on(
					"usuarioId" -> usuario_id,
					"credito" -> credito
				)
				.executeInsert()
			} catch {
				case e: Exception => {
					var s2 = SQL(
						"""
							UPDATE estudiante
							SET credito = credito + {credito}
							WHERE usuario_id = {usuarioId}
						"""
					)
					.on(
						"usuarioId" -> usuario_id,
						"credito" -> credito
					)
					.executeUpdate()
				}
			}
        }
		
		DB withConnection { implicit c =>
			var s1=SQL(
				"""
					UPDATE
						historial
					SET  status_id = 2
					WHERE usuario_id={id};
				"""
			)
			.on(
				"id" -> usuario_id
			)
			.executeInsert(scalar[Int].single)
		}

	}

	def denegar( usuario_id: Int ): Unit = {
		DB withConnection { implicit c =>
			var s1=SQL(
				"""
					UPDATE
						historial
					SET  status_id = 2
					WHERE usuario_id = {id};
				"""
			)
			.on(
				"id" -> usuario_id
			)
			.executeInsert()
		}
	}
}