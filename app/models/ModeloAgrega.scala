package models

import anorm._
import anorm.SqlParser._
import play.api.db.DB
import play.api.Play.current
import models.base._
import java.util.List
import scala.collection.JavaConversions._


class ModeloAgrega {

	def guardaImagenes( imagenes: List[String], videojuego_id: Int ) : Unit = {
		var rawSQL = "INSERT INTO imagen_juego VALUES "
		for( imagen <- imagenes )
			rawSQL += s"($videojuego_id, '$imagen'),"
		rawSQL = rawSQL.dropRight(1)
		DB withConnection { implicit c =>
			SQL( rawSQL ).execute()
		}
	}

	def borrarImagenes( imagenes: List[String], videojuego_id: Int ) : Unit = {
		var rawSQL = s"DELETE FROM imagen_juego WHERE videojuego_id = $videojuego_id AND imagen IN ("
		for( imagen <- imagenes )
			rawSQL += s"'$imagen',"
		rawSQL = rawSQL.dropRight(1)
		rawSQL += ")"
		DB withConnection { implicit c =>
			SQL( rawSQL ).execute()
		}
	}

	def guardaJuego(juego: Videojuego) : Int = {
		var videojuego_id = 0;
		DB withConnection { implicit c =>
			videojuego_id = SQL(
				"""
					INSERT INTO 
						videojuego
					VALUES 
						(
							DEFAULT, 
							{usuarioId}, 
							{nombre},
							to_tsvector( 'spanish', {nombre} ),
							{video},
							NOW(),
							{descripcion},
							{precio},
							{juego}
						)
				"""
			)
			.on(
				"usuarioId" -> juego.usuarioId,
				"nombre" -> juego.nombre,
				"video" -> juego.video,
				"descripcion" -> juego.descripcion,
				"precio" -> juego.precio,
				"juego" -> juego.juego
			)
			.executeInsert(scalar[Int].single)
		}
		return videojuego_id;
	}

	def actualizaJuego(juego : Videojuego) : Int = {
		var juego_id = 0
		
		DB withConnection { implicit c =>
			juego_id = SQL(
				"""
					UPDATE videojuego
					SET	nombre = {nombre},
						busqueda_nombre = to_tsvector( 'spanish', {nombre} ),
						video = {video},
						descripcion = {descripcion},
						precio = {precio},
						juego = {juego}
					WHERE id = {id_juego}
				"""
			)
			.on(
				"id_juego" -> juego.id,
				"nombre" -> juego.nombre,
				"video" -> juego.video,
				"descripcion" -> juego.descripcion,
				"precio" -> juego.precio,
				"juego" -> juego.juego
			)
			.executeInsert(scalar[Int].single)
		}
		return juego_id
	}
}