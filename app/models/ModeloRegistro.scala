package models

import anorm._
import anorm.SqlParser._
import play.api.db.DB
import play.api.Play.current
import models.base._

class ModeloRegistro {

	def guardaUsuario(usuario : Usuario) : Int = {
		var usuario_id = 0
		var usuario_datos = 0

		DB withConnection { implicit c =>
			usuario_id = SQL(
				"""
					INSERT INTO 
						usuario 
					VALUES 
						(DEFAULT, {username}, {password})
				"""
			)
			.on(
				"username" -> usuario.username,
				"password" -> usuario.password
			)
			.executeInsert(scalar[Int].single)
		}

		DB withConnection { implicit c =>
			SQL(
				"""
					INSERT INTO 
						estudiante 
					VALUES 
						({usuario_id}, 0)
				"""
			)
			.on(
				"usuario_id" -> usuario_id
			)
			.executeUpdate()
		}

		if( usuario_id >  0 ){
			DB withConnection { implicit c =>
				usuario_datos = SQL(
					"""
						INSERT INTO 
							usuario_datos 
						VALUES 
							({usuario_id},
							{nombre}, 
							{apellidoPaterno},
							{apellidoMaterno},
							{correo},
							{edad},
							{escuela},
							{promedio})
					"""
				)
				.on(
					"usuario_id" -> usuario_id,
					"username" -> usuario.username,
					"password" -> usuario.password,
					"nombre" -> usuario.nombre,
					"apellidoPaterno" -> usuario.apellidoPaterno,
					"apellidoMaterno" -> usuario.apellidoMaterno,
					"correo" -> usuario.mail,
					"edad" -> usuario.edad,
					"escuela" -> usuario.escuela,
					"promedio" -> usuario.promedio
				)
				.executeInsert(scalar[Int].single)
			}
		}

		if( usuario_id > 0 ) {
			DB withConnection { implicit c =>
				val semestre_id = SQL(
					"""
						SELECT semestre.id
						FROM semestre JOIN status ON status.id = semestre.status_id
						WHERE status.clave = 'A'
					"""
				).as( scalar[Int].single )

				SQL(
					"""
						INSERT INTO
							historial
						VALUES (
							{usuario_id},
							{semestre_id},
							3,
							{historial}
						)
					"""
				).on(
					"usuario_id" -> usuario_id,
					"semestre_id" -> semestre_id,
					"historial" -> usuario.historial
				).executeInsert()
			}
		}
		return usuario_datos
	}

	def actualizaUsuario(usuario : Usuario) : Int = {
		var usuario_id = 0

		DB withConnection { implicit c =>
			usuario_id = SQL(
				"""
					UPDATE usuario_datos
					SET nombre = {nombre}, 
						apellido_paterno = {apellidoPaterno},
						apellido_materno = {apellidoMaterno},
						mail = {correo},
						edad = {edad},
						escuela = {escuela},
						promedio = {promedio}
					WHERE usuario_id = {id_usuario}
				"""
			)
			.on(
				"nombre" -> usuario.nombre,
				"apellidoPaterno" -> usuario.apellidoPaterno,
				"apellidoMaterno" -> usuario.apellidoMaterno,
				"correo" -> usuario.mail,
				"edad" -> usuario.edad,
				"escuela" -> usuario.escuela,
				"promedio" -> usuario.promedio,
				"id_usuario" -> usuario.id
			)
			.executeInsert(scalar[Int].single)
		}
		return usuario_id
	}

	def recuperaUsuario(id_usuario : Integer) : Usuario = {
		var usuario: Usuario = null

		DB withConnection { implicit c =>
			val usuarioSQL = SQL(
				"""
					SELECT
						*
					FROM
						usuario JOIN usuario_datos ON id = usuario_id
					WHERE
						id = {id_usuario}
				"""
			)
			.on(
				"id_usuario" -> id_usuario
			)
			.apply
			.headOption
			
			usuario = usuarioSQL match {
				case Some(tupla) => 
					new Usuario(
						tupla[Int]("id"),
						tupla[String]("username"),
						tupla[String]("password"),
						tupla[String]("nombre"),
						tupla[String]("apellido_paterno"),
						tupla[String]("apellido_materno"),
						tupla[String]("mail"),
						tupla[Int]("edad"),
						tupla[String]("escuela"),
						tupla[Double]("promedio")
					)
				case _ => null
			}
		}
		return usuario
	}

}