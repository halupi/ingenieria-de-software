package models

import anorm._
import anorm.SqlParser._
import play.api.db.DB
import play.api.Play.current
import models.base._

class ModeloLogin {

	def obtenerUsuario(username: String, password: String): Int = {
		var usuario_id = 0

		DB withConnection { implicit c =>
			val resultado: Option[Int] = SQL(
				"""
					SELECT
						id
					FROM
						usuario
					WHERE
						username = {username} AND password = {password}
				"""
			)
			.on(
				"username" -> username,
				"password" -> password
			).as( scalar[Int].singleOpt )

			usuario_id = resultado match {
				case Some(id) => id
				case None => 0
			}
		}

		return usuario_id
	}

}