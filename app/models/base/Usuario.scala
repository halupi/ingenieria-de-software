package models.base

import anorm._
import anorm.SqlParser._
import play.api.db.DB
import play.api.Play.current

import play.libs.Json
import com.fasterxml.jackson.databind.node.ObjectNode
import com.fasterxml.jackson.databind.node.JsonNodeFactory
import com.fasterxml.jackson.databind.node.ArrayNode

case class Usuario( id: Int, username: String, password: String, nombre: String, apellidoPaterno: String,
	apellidoMaterno: String, mail: String, edad: Int, escuela: String, promedio: Double, historial: String ){

	var creditos: Int = 0;

	def this( username: String, password: String, nombre: String, apellidoPaterno: String, 
		apellidoMaterno: String, mail: String, edad: Int, escuela: String, promedio: Double, historial: String) = 
			this(0, username, password, nombre, apellidoPaterno, apellidoMaterno, mail, edad, escuela, promedio, historial	)

	def this( username: String, password: String, nombre: String, apellidoPaterno: String, 
		apellidoMaterno: String, mail: String, edad: Int, escuela: String, promedio: Double) = 
			this(0, username, password, nombre, apellidoPaterno, apellidoMaterno, mail, edad, escuela, promedio, ""	)

	def this( id: Int, username: String, password: String, nombre: String, apellidoPaterno: String, 
		apellidoMaterno: String, mail: String, edad: Int, escuela: String, promedio: Double) = 
			this(id, username, password, nombre, apellidoPaterno, apellidoMaterno, mail, edad, escuela, promedio, "" )

	def toJSON() : ObjectNode = {
		val json: ObjectNode = Json.newObject();

		json.put("id", id);
		json.put("username", username);
		json.put("password", password);
		json.put("nombre", nombre);
		json.put("paterno", apellidoPaterno);
		json.put("materno", apellidoMaterno);
		json.put("correo", mail);
		json.put("edad", edad);
		json.put("escuela", escuela);
		json.put("promedio", promedio);
		json.put("creditos", creditos);
		json.put("historial", historial);

		return json;
	}

	def cargarCreditos() : Unit = {
		var creditos: Int = 0;

		DB withConnection { implicit c =>
			val resultado: Option[Int] = SQL(
				"""
					SELECT
						credito
					FROM
						estudiante
					WHERE
						usuario_id = {usuario_id}
				"""
			)
			.on(
				"usuario_id" -> id
			).as( scalar[Int].singleOpt )

			creditos = resultado match {
				case Some(numero) => numero
				case None => 0
			}
		}

		this.creditos = creditos;
	}

	def obtenerCreditos() : Int = this.creditos

	def asignarCreditos(creditos: Int) : Unit = this.creditos = creditos;

	def obtenerHistorial(): String = this.historial
}

object Usuario {

	def esAdmin(id: Int) : Boolean = {
		var usuario: Boolean = false

		DB withConnection { implicit c =>
			val usuarioSQL = SQL(
				"""
					SELECT
						*
					FROM
						usuario JOIN desarrollador ON id = usuario_id
					WHERE
						id = {id}
				"""
			)
			.on(
				"id" -> id
			)
			.apply
			.headOption
			
			usuario = usuarioSQL match {
				case Some(tupla) => true
				case _ => false
			}
		}

		return usuario
	}

	def esAdmin(id: String): Boolean = {
		return id != null && esAdmin( Integer.parseInt(id) );
	}

	def obtenerPorId(id: Int) : Usuario = {
		var usuario: Usuario = null

		DB withConnection { implicit c =>
			val usuarioSQL = SQL(
				"""
					SELECT
						*
					FROM
						usuario JOIN usuario_datos ON id = usuario_datos.usuario_id
						LEFT JOIN historial ON id = historial.usuario_id
					WHERE
						id = {id}
				"""
			)
			.on(
				"id" -> id
			)
			.apply
			.headOption
			
			usuario = usuarioSQL match {
				case Some(tupla) => {
					val url: Option[String] = tupla[Option[String]]("url");
					new Usuario(
						tupla[Int]("id"),
						tupla[String]("username"),
						tupla[String]("password"),
						tupla[String]("nombre"),
						tupla[String]("apellido_paterno"),
						tupla[String]("apellido_materno"),
						tupla[String]("mail"),
						tupla[Int]("edad"),
						tupla[String]("escuela"),
						tupla[Double]("promedio"),
						url getOrElse ""
					)
				}
				case _ => null
			}
		}

		return usuario
	}
}