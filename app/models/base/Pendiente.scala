package models.base

import anorm._
import anorm.SqlParser._
import play.api.db.DB
import play.api.Play.current
import play.libs.Json
import java.util.List
import scala.collection.JavaConverters._
import com.fasterxml.jackson.databind.node.ObjectNode
import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.fasterxml.jackson.databind.node.ArrayNode;

case class Pendiente( id: Int, username: String, nombre: String, apellidoPaterno: String,
	apellidoMaterno: String, mail: String, edad: Int, escuela: String, promedio: Double, url: String, credito: Int){

	//def this( id: Int, username: String, nombre: String, apellidoPaterno: String,apellidoMaterno: String, mail: String, edad: Int, escuela: String, promedio: Double, url: String, credito: Int)=this(id,username,nombre,apellidoPaterno,apellidoMaterno,mail,edad,escuela,promedio,url,0)

	def toJSON():ObjectNode = {
		val json:ObjectNode = Json.newObject();
		json.put("id", id);
		json.put("username", username);
		json.put("nombre", nombre);
		json.put("apellidoPaterno", apellidoPaterno);
		json.put("apellidoMaterno", apellidoMaterno);
		json.put("mail", mail);
		json.put("edad", edad);
		json.put("escuela", escuela);
		json.put("promedio", promedio);
		json.put("url", url);
		json.put("credito", credito);
		return json;
	}		
}