package models.base

import anorm._
import anorm.SqlParser._
import play.api.db.DB
import play.api.Play.current
import play.libs.Json
import java.util.List
import scala.collection.JavaConverters._
import com.fasterxml.jackson.databind.node.ObjectNode
import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.fasterxml.jackson.databind.node.ArrayNode;
import java.util.Date

case class Videojuego( id: Int, usuarioId: Int, nombre: String, descripcion: String, precio: Int,
		video: String, publicacion: String ){

	var imagenes: List[String] = null

   var juego: String = null

   var total: Long = 0

	def this( usuarioId: Int, nombre: String, descripcion: String, precio: Int,
		video: String, publicacion: String ) = 
			this(0, usuarioId, nombre, descripcion, precio, video, publicacion)

   def asignarJuego( juego: String ) : Videojuego = {
      this.juego = juego
      return this;
   }

   def obtenerJuego(): String = return this.juego
	
	def toJSON(): ObjectNode = {
   		val json: ObjectNode = Json.newObject();
   		val arregloImagenes = new ArrayNode( JsonNodeFactory.instance );
   		json.put("id", id)
   		json.put("usuario_id", usuarioId)
   		json.put("nombre", nombre)
   		json.put("descripcion", descripcion)
   		json.put("precio", precio)
   		json.put("video", video)
         json.put("publicacion", publicacion)
         json.put("total", total)

   		if( imagenes != null ) {
   			for( imagen <- imagenes.asScala )
   				arregloImagenes.add( imagen )
   			json.put("imagenes", arregloImagenes)
   		}

         if( juego != null )
            json.put("juego", juego);

   		return json;
   }

   def cargarImagenes() : Unit = {
      var imagenes: List[String] = null;

      DB withConnection { implicit c =>
         val query = SQL( "SELECT imagen FROM imagen_juego WHERE videojuego_id = {videojuego_id}" )

         imagenes = query.on( "videojuego_id" -> id ).as( scalar[String].* ).asJava
      }

      this.imagenes = imagenes;
   }

   def obtenerPrecio() : Int = this.precio
   def obtenerImagenes() : List[String] = this.imagenes

   def asignarTotal(total: Long): Videojuego = {
      this.total = total
      return this
   }

}

object Videojuego {
   val PAGINADOR = 10

   def busqueda( texto: String ) : List[Videojuego] = {
      var palabras: Array[String] = texto split " ";
      var tsvector: String = palabras.mkString(" | ");
      var juegos: List[Videojuego] = null;
      
      DB withConnection { implicit c =>
          juegos = SQL(
            """
               SELECT
                  *, ts_rank(busqueda_nombre, keywords, 1) as rank
               FROM
                  Videojuego, to_tsquery({ts_vector}) keywords
               WHERE
                  keywords @@ busqueda_nombre
            """
         )
         .on( "ts_vector" -> tsvector )().map(row => new Videojuego(
					row[Int]("id"),
					row[Int]("usuario_id"),
					row[String]("nombre"),
					row[String]("descripcion"),
					row[Int]("precio"),
					row[String]("video"),
               row[Date]("publicacion").toString()
         	).asignarJuego( row[String]("juego") )
         ).toList.asJava
      }

      return juegos;
   }

   def catalogoCompleto(p: Int): List[Videojuego] = {
      var juegos: List[Videojuego] = null;

      DB withConnection { implicit c =>
         val sql = SQL(
            """
               SELECT *, count(*) OVER() as total
               FROM
                  videojuego
               LIMIT {paginador}
               OFFSET {index}
            """
         )

         juegos = sql.on( 
            "index" -> (p-1)*PAGINADOR, 
            "paginador" -> PAGINADOR 
         )().map(row => new Videojuego(
               row[Int]("id"),
               row[Int]("usuario_id"),
               row[String]("nombre"),
               row[String]("descripcion"),
               row[Int]("precio"),
               row[String]("video"),
               row[Date]("publicacion").toString()
            )
            .asignarJuego( row[String]("juego") )
            .asignarTotal( row[Long]("total") )
         ).toList.asJava
      }

      return juegos;
   }

   def catalogoVendidos(p: Int): List[Videojuego] = {
      var juegos: List[Videojuego] = null;

      DB withConnection { implicit c =>
         val sql = SQL(
            """
               SELECT id, videojuego.usuario_id, nombre, precio,
                  juego, descripcion, video, publicacion,
                  SUM(precio) as monto, count(*) OVER() as total
               FROM
                  videojuego JOIN compra ON id = videojuego_id
               GROUP BY id
               ORDER BY monto DESC
               LIMIT {paginador}
               OFFSET {index}
            """
         )

         juegos = sql.on( 
            "index" -> (p-1)*PAGINADOR,
            "paginador" -> PAGINADOR
         )().map(row => new Videojuego(
               row[Int]("id"),
               row[Int]("usuario_id"),
               row[String]("nombre"),
               row[String]("descripcion"),
               row[Int]("precio"),
               row[String]("video"),
               row[Date]("publicacion").toString()
            )
            .asignarJuego( row[String]("juego") )
            .asignarTotal( row[Long]("total") )
         ).toList.asJava
      }

      return juegos;
   }

   def catalogoRecientes(p: Int): List[Videojuego] = {
      var juegos: List[Videojuego] = null;

      DB withConnection { implicit c =>
         val sql = SQL(
            """
               SELECT *, count(*) OVER() as total
               FROM
                  videojuego
               ORDER BY publicacion DESC
               LIMIT {paginador}
               OFFSET {index}
            """
         )

         juegos = sql.on( 
            "index" -> (p-1)*PAGINADOR,
            "paginador" -> PAGINADOR
         )().map(row => new Videojuego(
               row[Int]("id"),
               row[Int]("usuario_id"),
               row[String]("nombre"),
               row[String]("descripcion"),
               row[Int]("precio"),
               row[String]("video"),
               row[Date]("publicacion").toString()
            )
            .asignarJuego( row[String]("juego") )
            .asignarTotal( row[Long]("total") )
         ).toList.asJava
      }

      return juegos;
   }

   def catalogoGratuitos(p: Int): List[Videojuego] = {
      var juegos: List[Videojuego] = null;

      DB withConnection { implicit c =>
         val sql = SQL(
            """
               SELECT *, count(*) OVER() as total
               FROM
                  videojuego
               WHERE precio = 0
               LIMIT {paginador}
               OFFSET {index}
            """
         )

         juegos = sql.on( 
            "index" -> (p-1)*PAGINADOR,
            "paginador" -> PAGINADOR
         )().map(row => new Videojuego(
               row[Int]("id"),
               row[Int]("usuario_id"),
               row[String]("nombre"),
               row[String]("descripcion"),
               row[Int]("precio"),
               row[String]("video"),
               row[Date]("publicacion").toString()
            )
            .asignarJuego( row[String]("juego") )
            .asignarTotal( row[Long]("total") )
         ).toList.asJava
      }

      return juegos;
   }

   def obtenerComprado(videojuego_id: Int, usuario_id: Int): Videojuego = {
      var juego: Videojuego = null

      DB withConnection { implicit c =>
         val sql = SQL(
            """
               SELECT
                  id, videojuego.usuario_id, nombre, descripcion, precio, video, publicacion, juego
               FROM
                  videojuego JOIN compra ON id = videojuego_id
               WHERE
                  compra.videojuego_id = {videojuego_id}
               AND
                  compra.usuario_id = {usuario_id}
            """
         )
         .on(
            "videojuego_id" -> videojuego_id,
            "usuario_id" -> usuario_id
         )
         .apply
         .headOption
         
         juego = sql match {
            case Some(tupla) => new Videojuego(
               tupla[Int]("id"),
               tupla[Int]("usuario_id"),
               tupla[String]("nombre"),
               tupla[String]("descripcion"),
               tupla[Int]("precio"),
               tupla[String]("video"),
               tupla[Date]("publicacion").toString()
            ).asignarJuego( tupla[String]("juego") )
            case _ => null
         }
      }

      return juego;
   }

   def obtenerPorId(id: Int) : Videojuego = {
      var videojuego: Videojuego = null

      DB withConnection { implicit c =>
         val sql = SQL(
            """
               SELECT
                  *
               FROM
                  videojuego
               WHERE id = {id}
            """
         )
         .on(
            "id" -> id
         )
         .apply
         .headOption
         
         videojuego = sql match {
            case Some(tupla) => new Videojuego(
                  tupla[Int]("id"),
                  tupla[Int]("usuario_id"),
                  tupla[String]("nombre"),
                  tupla[String]("descripcion"),
                  tupla[Int]("precio"),
                  tupla[String]("video"),
                  tupla[Date]("publicacion").toString()
               ).asignarJuego( tupla[String]("juego") )
            case _ => null
         }
      }

      return videojuego
   }
}