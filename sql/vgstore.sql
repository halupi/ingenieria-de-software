CREATE TABLE usuario(
	id SERIAL,
	username varchar(50),
	password varchar(50),
	UNIQUE(username),
	PRIMARY KEY(id)
);

CREATE TABLE usuario_datos(
	usuario_id int,
	nombre varchar(100) NOT NULL,
	apellido_paterno varchar(100) NOT NULL,
	apellido_materno varchar(100),
	mail varchar(100) NOT NULL,
	edad int NOT NULL,
	escuela varchar(100) NOT NULL,
	promedio numeric NOT NULL,
	UNIQUE(mail),
	PRIMARY KEY(usuario_id),
	FOREIGN KEY(usuario_id) REFERENCES usuario(id) 
		ON UPDATE CASCADE ON DELETE CASCADE
);

CREATE TABLE videojuego(
	id SERIAL,
	usuario_id int,
	nombre varchar(150) NOT NULL,
	busqueda_nombre tsvector NOT NULL,
	video varchar(100) NOT NULL,
	publicacion date NOT NULL,
	descripcion text NOT NULL,
	precio int NOT NULL,
	juego varchar(255) NOT NULL,
	PRIMARY KEY(id),
	FOREIGN KEY(usuario_id) REFERENCES usuario(id)
		ON UPDATE CASCADE ON DELETE CASCADE
);

CREATE TABLE imagen_juego(
	videojuego_id int,
	imagen varchar(150) NOT NULL,
	PRIMARY KEY(videojuego_id, imagen),
	FOREIGN KEY(videojuego_id) REFERENCES videojuego(id)
		ON UPDATE CASCADE ON DELETE CASCADE
);

CREATE TABLE compra(
	usuario_id int,
	videojuego_id int,
	fecha timestamp,
	PRIMARY KEY(usuario_id, videojuego_id),
	FOREIGN KEY(usuario_id) REFERENCES usuario(id)
		ON UPDATE CASCADE ON DELETE CASCADE,
	FOREIGN KEY(videojuego_id) REFERENCES videojuego(id)
);

CREATE TABLE status(
	id SERIAL,
	clave char(1),
	descripcion varchar(20),
	PRIMARY KEY(id)
);

CREATE TABLE semestre(
	id SERIAL,
	status_id int,
	clave char(6) NOT NULL,
	PRIMARY KEY(id),
	FOREIGN KEY(status_id) REFERENCES status(id)
		ON UPDATE CASCADE ON DELETE CASCADE
);

CREATE TABLE historial(
	usuario_id int,
	semestre_id int,
	status_id int,
	url varchar(100) NOT NULL,
	PRIMARY KEY(usuario_id, semestre_id),
	FOREIGN KEY(usuario_id) REFERENCES usuario(id)
		ON UPDATE CASCADE ON DELETE CASCADE,
	FOREIGN KEY(semestre_id) REFERENCES semestre(id)
		ON UPDATE CASCADE ON DELETE CASCADE,
	FOREIGN KEY(status_id) REFERENCES status(id)
		ON UPDATE CASCADE ON DELETE CASCADE
);

CREATE TABLE desarrollador(
	usuario_id int,
	PRIMARY KEY(usuario_id),
	FOREIGN KEY(usuario_id) REFERENCES usuario(id) 
		ON UPDATE CASCADE ON DELETE CASCADE
);

CREATE TABLE estudiante(
	usuario_id int,
	credito int,
	CHECK(credito >= 0),
	PRIMARY KEY(usuario_id),
	FOREIGN KEY(usuario_id) REFERENCES usuario(id) 
		ON UPDATE CASCADE ON DELETE CASCADE
);

INSERT INTO usuario VALUES(DEFAULT, 'admin', 'admin');
INSERT INTO usuario_datos VALUES(currval('usuario_id_seq'), 'Administrador', '', '', 'admin@admin.com', 23, 'Facultad de Ciencias, UNAM', 10);
INSERT INTO desarrollador VALUES(currval('usuario_id_seq'));