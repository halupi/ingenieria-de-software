INSERT INTO videojuego VALUES 
(DEFAULT, 1, 'Grand Theft Auto V',
to_tsvector('spanish','Grand Theft Auto V joven estafador, un ladrón de bancos retirado y un terrorífico psicópata'),
'nTTdpA1lGTY',
'2015/04/14', 
'Cuando un joven estafador, un ladrón de bancos retirado y un terrorífico psicópata se ven mezclados con algunos de los sujetos más temibles y perturbados de los bajos fondos, el gobierno de EE.UU. y la industria del entretenimiento, deben dar una serie de golpes para sobrevivir en una ciudad sin ley donde no pueden confiar en nadie, ni si quiera entre ellos.',
800, 'GTAV.jpg');

INSERT INTO imagen_juego VALUES
(currval('videojuego_id_seq'), 'GTAV.jpg');

INSERT INTO videojuego VALUES 
(DEFAULT, 1, 'Metal Gear Solid V: Ground Zeroes',
to_tsvector('spanish','Metal Gear Solid V: Ground Zeroes El conocido estudio Kojima Productions, de Konami Digital Entertainment, ha publicado una nueva obra maestra de la serie Metal Gear Solid con el título METAL GEAR SOLID V: GROUND ZEROES. Se trata de la primera parte de la serie "Metal Gear Solid V Experience" y sirve de prólogo a una segunda entrega más larga que se publicará con posterioridad, METAL GEAR SOLID V: THE PHANTOM PAIN'),
'ltH1eWxZutE',
'2014/12/18', 
'El conocido estudio Kojima Productions, de Konami Digital Entertainment, ha publicado una nueva obra maestra de la serie Metal Gear Solid con el título METAL GEAR SOLID V: GROUND ZEROES. Se trata de la primera parte de la serie "Metal Gear Solid V Experience" y sirve de prólogo a una segunda entrega más larga que se publicará con posterioridad, METAL GEAR SOLID V: THE PHANTOM PAIN.

METAL GEAR SOLID V: GROUND ZEROES proporciona a los seguidores acérrimos de la serie un aperitivo del impresionante despliegue visual y técnico que brinda esta gran superproducción antes del lanzamiento del juego principal. También ofrece, tanto a los veteranos como a aquellos que nunca han se han enfrentado a un título de Kojima Productions, la oportunidad de familiarizarse con un diseño y un estilo de juego radicalmente nuevos.

La franquicia Metal Gear Solid lleva décadas revolucionando la industria de los videojuegos con el aplauso de la crítica y el público. Ahora, Kojima Productions vuelve a subir el listón utilizando el motor FOX para ofrecer una increíble fidelidad de juego e introducir un mundo abierto en el universo de la saga. Es la experiencia que los fans estaban esperando.',
1200, 'MGSV_Ground_Zeroes_boxart.jpg');

INSERT INTO imagen_juego VALUES
(currval('videojuego_id_seq'), 'MGSV_Ground_Zeroes_boxart.jpg');

INSERT INTO videojuego VALUES 
(DEFAULT, 1, 'Elite Dangerous',
to_tsvector('spanish','Elite Dangerous Ponte al mando de tu propia nave en una galaxia implacable. Corre el año 3300 y en la vasta inmensidad de la Vía Láctea'),
'Ei_Y1_cpJX4',
'2014/12/16', 
'Ponte al mando de tu propia nave en una galaxia implacable. Corre el año 3300 y en la vasta inmensidad de la Vía Láctea (reproducida a escala) afloran las rivalidades entre los planetas instigadas por superpotencias galácticas que libran una guerra encubierta. Algunos te considerarán su aliado, otros dirán que eres pirata, cazarrecompensas, contrabandista, explorador, asesino, héroe... Vuela en solitario o en compañía de tus amigos, lucha por una causa o sigue tu propio camino; tus acciones cambiarán la galaxia en una historia sin fin.',
450, 'EliteDangerous.jpg');

INSERT INTO imagen_juego VALUES
(currval('videojuego_id_seq'), 'EliteDangerous.jpg');

INSERT INTO videojuego VALUES 
(DEFAULT, 1, 'Project Cars',
to_tsvector('spanish','Project Cars forma insuperable de vivir las carreras'),
'DtvFNKmS3qs',
'2015/05/06', 
'Dirigido, probado y aprobado por pilotos reales y una apasionada comunidad de amantes de las competiciones de motor, Project CARS abre paso a una nueva generación de los simuladores de carreras que combina como ningún otro la pericia de los mejores desarrolladores con unos contenidos hechos a la medida de los fans.

Descubre una forma insuperable de vivir las carreras con unos gráficos excepcionales y un sistema de control que te hará sentir como si estuvieses sobre el asfalto. Crea un conductor, elige la prueba que más te guste en el modo de carrera dinámica y escribe tu propia historia en una intensa competición multijugador online.

La parrilla más extensa del circuito, un revolucionario sistema de tiempo dinámico con cambios de luz y climatológicos, innumerables funciones de puesta a punto y parada en boxes, compatibilidad con cascos Oculus Rift y resoluciones ultra-HD (12K)... Project CARS tiene una mecánica que ninguno de sus competidos puede superar.',
360, 'PojectCars.jpg');

INSERT INTO imagen_juego VALUES
(currval('videojuego_id_seq'), 'PojectCars.jpg');

INSERT INTO videojuego VALUES 
(DEFAULT, 1, 'The Witcher 3: Wild Hunt',
to_tsvector('spanish','The Witcher 3: Wild Hunt Eres Geralt de Rivia, un cazador de monstruos encargado de encontrar a la "niña de la profecía'),
'tDfa1Qp2SwA',
'2015/05/19', 
'The Witcher® 3: Wild Hunt es un juego de rol de última generación que gira en torno a una historia basada en un mundo abierto y ambientada en un asombroso universo de fantasía en el que cada decisión que tomes tendrá consecuencias trascendentales. Eres Geralt de Rivia, un cazador de monstruos encargado de encontrar a la "niña de la profecía".

Geralt de Rivia es un brujo, un cazador de monstruos por encargo, un personaje legendario de pelo blanco y ojos felinos. Entrenados desde la niñez y transformados para adquirir reflejos y poderes sobrehumanos, los brujos como Geralt son una consecuencia natural de un mundo poblado de monstruos que es necesario equilibrar. Encarnado en Geralt, te embarcarás en un viaje épico por un territorio devastado por la guerra que te conducirá inevitablemente al combate con el enemigo más oscuro al que se haya enfrentado jamás la humanidad: la Cacería Salvaje.',
630, 'TheWitcher.jpg');

INSERT INTO imagen_juego VALUES
(currval('videojuego_id_seq'), 'TheWitcher.jpg');

INSERT INTO videojuego VALUES 
(DEFAULT, 1, 'Wolfenstein®: The Old Blood™',
to_tsvector('spanish','Wolfenstein: The Old Blood Corre el año 1946 y los Nazis están a punto de ganar la II Guerra Mundial'),
'Viv8e9_-kHo',
'2015/05/05', 
'Corre el año 1946 y los Nazis están a punto de ganar la II Guerra Mundial. En el intento de cambiar las tornas en favor de los Aliados, B.J. Blazkowicz debe emprender una misión épica dividida en dos partes que le conducirá al interior de Baviera….

En la primera parte, Rudi Jäger y La guarida de lobos, B.J. Blazkowicz se enfrenta a un alcaide maníaco cuando se infiltra en el castillo Wolfenstein para intentar robar las coordenadas de las instalaciones del general Calavera. En la segunda parte, titulada Los oscuros secretos de Helga Von Schabbs, la búsqueda de las coordenadas lleva a nuestro héroe hasta la ciudad de Wulfburg, donde una obsesa arqueóloga nazi está desenterrando unos artefactos misteriosos que amenazan con liberar un oscuro poder ancestral.',
935, 'Wolfenstein.jpg');

INSERT INTO imagen_juego VALUES
(currval('videojuego_id_seq'), 'Wolfenstein.jpg');

INSERT INTO videojuego VALUES 
(DEFAULT, 1, 'Battlefield Hardline',
to_tsvector('spanish','Battlefield Hardline La lucha entre policías y ladrones se pone al rojo vivo gracias a los creadores de las sagas Battlefield'),
'dJjwjMhqNEM',
'2015/03/20', 
'La lucha entre policías y ladrones se pone al rojo vivo gracias a los creadores de las sagas Battlefield y Dead Space. En el entramado de calles de la jungla urbana, la estrategia y las tácticas son tan vitales como la velocidad con el gatillo. Los accesorios, el equipamiento y los vehículos de alta velocidad elevan el shooter en primera persona a nuevas cotas de tensión.

Hay un furgón de seguridad con sacas de dinero aparcado en un garaje y solo tienes unos segundos para cargar tanta pasta como puedas en la mochila antes de que llegue la poli. Vas dejando un rastro de billetes detrás de ti mientras corres a toda pastilla hacia los ascensores. Cuando llegas a la azotea, un miembro de tu equipo utiliza la tirolina, pero tú eliges un camino más directo y te lanzas al vacío. Se abre el paracaídas. Las balas pasan demasiado cerca. Cuando tocas el suelo, sales pitando hacia el vehículo de huida. Suenan las sirenas. La policía está en camino. Justo en ese momento te das cuenta de que está a punto de caer una enorme grúa. Es un día cualquiera en la vida de un jugador de Battlefield Hardline.

Policías y ladrones – El salto de los combates militares al mundo de la delincuencia abre paso a una nueva entrega de Battlefield. Deberás robar cámaras acorazadas en lugar de capturar banderas y cruzar de un edificio a otro en tirolina mientras suenan los ecos de las sirenas por la ciudad. La estética, los escenarios, todo se ha adaptado para dar nuevos aires a la franquicia.

La auténtica experiencia Battlefield – Sí, vas a cambiar las zonas de combate de otros países por escenarios urbanos, pero detrás de cada disparo encontrarás la experiencia multijugador que esperas de un juego Battlefield. Los entornos destructibles, el armamento moderno, los vehículos de altas prestaciones y los grandes enfrentamientos te harán sentirte como en casa.

Una campaña de lujo – Elaborado por Visceral, autores de la trilogía Dead Space, el modo solitario de Hardline es como vivir una de las grandes series policíacas de la tele. Los niveles van avanzando a lo largo de la historia como en los episodios de estas series y los momentos cumbre del guión desembocan en secuencias de juego memorables. ¿Persecuciones de vértigo y enfrentamientos a tiros? Sí, por favor.',
1550, 'battlefield-hardline.jpg');

INSERT INTO imagen_juego VALUES
(currval('videojuego_id_seq'), 'battlefield-hardline.jpg');

INSERT INTO videojuego VALUES 
(DEFAULT, 1, 'Batman®: Arkham Knight',
to_tsvector('spanish','Batman: Arkham Knight universo de Batman que alcanza su máxima expresión cuando lo vemos surcar veloz las calles y los cielos de Gotham'),
't6EgqHYxFr8',
'2015/06/23', 
'Batman™: Arkham Knight pone un final épico a la premiada trilogía de Rocksteady Studios. Desarrollada únicamente para plataformas de nueva generación, esta entregaincluye la esperada versión del Batmóvil diseñada por Rocksteady. La introducción de este legendario vehículo unida a la popular mecánica de juego de la serie Arkham dan como resultado una experiencia única y completa del universo de Batman que alcanza su máxima expresión cuando lo vemos surcar veloz las calles y los cielos de Gotham. En este explosivo final, Batman se enfrenta a la amenaza más letal que se haya cernido jamás sobre la ciudad que ha jurado proteger: el Espantapájaros ha vuelto para unir a los supervillanos de Gotham y destruir al Caballero Oscuro para siempre.',
1310, 'Batman.jpg');

INSERT INTO imagen_juego VALUES
(currval('videojuego_id_seq'), 'Batman.jpg');

INSERT INTO videojuego VALUES 
(DEFAULT, 1, 'Battlefield 4',
to_tsvector('spanish','Battlefield 4 evacuación de importantes personalidades estadounidenses de Shanghái y sigue los esfuerzos de tu pelotón para lograr volver a casa'),
'8BcFSHW6iTE',
'2015/04/14', 
'Battlefield 4™ es la superproducción de acción que redefine el género, creada a partir de momentos que borran la línea que separa un juego de la gloria. Alimentado por la potencia y la fidelidad de próxima generación de Frostbite™ 3, Battlefield 4™ proporciona una experiencia visceral y dramática sin igual.
Sólo en Battlefield puedes demoler los edificios que protegen a tu enemigo. Sólo en Battlefield liderarás un asalto desde la parte trasera de una patrullera. Battlefield te ofrece la libertad para hacer más y ser más, mientras sacas partido a tus puntos fuertes y te abres camino hacia la victoria.
Además del sello de su modo multijugador, Battlefield 4™ presenta una campaña intensa y dramática centrada en los personajes, que comienza con la evacuación de importantes personalidades estadounidenses de Shanghái y sigue los esfuerzos de tu pelotón para lograr volver a casa.
No hay nada comparable. Sumérgete en el glorioso caos de la guerra total, sólo en Battlefield.',
1459, 'Battlefield4.jpg');

INSERT INTO imagen_juego VALUES
(currval('videojuego_id_seq'), 'Battlefield4.jpg');

INSERT INTO videojuego VALUES 
(DEFAULT, 1, 'Gas Guzzlers: Combat Carnage',
to_tsvector('spanish','Gas Guzzlers: Combat Carnage Batalla de autos con grandes mejoras en cuanto aquipamiento y pistas'),
'dY4nugKIhRs',
'2012/05/18', 
'Incluye gran número de vehículos diferentes, así como gran variedad de armas, mejoras, bonificaciones en pista y entornos de juego sorprendentes. El jugador empieza con un vehículo de poca potencia y va mejorándolo gradualmente según gana dinero en una serie de carreras de infarto. Al principio dispone de una cantidad de dinero con la que compra su primer coche sin mejoras.

A partir de ahí, se suceden una serie de eventos y retos que se dividen en cuatro categorías diferentes. Tanto las categorías como los circuitos y los vehículos se van desbloqueando a medida que el jugador progresa en el juego. Después de seleccionar la clase (sólo hay una disponible al principio), viene la selección de eventos. Al principio del juego, el número de eventos es limitado, pero va aumentando con el tiempo, sobre todo en función de la puntuación conseguida. Los eventos desbloqueados pueden ejecutarse un número ilimitado de veces y, cada una de ellas, el jugador gana puntos y dinero para el evento seleccionado.

El cambio de una la inferior a la superior se consigue ganando el torneo especial. Además de los jugadores, en los eventos y las competiciones participan varios pilotos manejados por la IA. Todos los pilotos basados en la IA se simulan con arreglo a sus propios parámetros y ajustes. De hecho, pueden progresar y mejorar sus vehículos de una forma muy similar a la del jugador, una peculiaridad que añade realismo al juego. En cada categoría hay un ranking oficial que muestra la posición actual de todos los pilotos.

El jugador debe obtener victorias de forma regular para seguir en cabeza, así como ganar dinero para mantener su vehículo y ahorrar para uno nuevo. Todas las mejoras llevan asociado un precio que aumenta en función del tipo de coche. Los eventos/competiciones normales son carreras, carrera a muerte (Battle Race), carreras con vueltas eliminatorias (Knock-outs) y torneos.

Además hay numerosos eventos desconocidos, coches secretos y otros elementos misteriosos del juego, ¡pero ninguno de ellos se puede desvelar!',
0, 'GasGuzzlers.jpg');

INSERT INTO imagen_juego VALUES
(currval('videojuego_id_seq'), 'GasGuzzlers.jpg');

INSERT INTO videojuego VALUES 
(DEFAULT, 1, 'Portal 2',
to_tsvector('spanish','Portal 2 Juego con ingeniosos puzles y sus atractivos diálogos'),
'dWTqylt3JQQ',
'2011/04/21', 
'Seguramente recordarás aquella primera versión de Portal publicada en 2007 e incluida en el novedoso paquete de juegos "Orange Box" de Valve. Aunque el juego causó sensación por su sentido innovador, sus ingeniosos puzles y sus atractivos diálogo, mereció una crítica importante. Sus innumerables seguidores se lamentaban de que “era demasiado corto". La mayoría de ellos conseguían terminarlo en cinco horas. Valve ha corregido el problema con una historia independiente y de mayor duración. Además de ofrecer una campaña de juego en solitario más jugosa, Portal 2 prolonga las horas de diversión con un modo cooperativo para dos jugadores.',
0, 'Portal2cover.jpg');

INSERT INTO imagen_juego VALUES
(currval('videojuego_id_seq'), 'Portal2cover.jpg');

INSERT INTO videojuego VALUES 
(DEFAULT, 1, 'Infinite Crisis',
to_tsvector('spanish','Infinite Crisis Dos grupos de héroes y villanos de DC compiten en diferentes pruebas y múltiples terrenos de combate donde eventos catastróficos cambian el devenir del juego'),
'nTTdpA1lGTY',
'2015/04/14', 
'Infinite Crisis (IC) combina los géneros de rol y estrategia para crear una palpitante aventura online ambientada en el multiverso de DC Comics. Dos grupos de héroes y villanos de DC compiten en diferentes pruebas y múltiples terrenos de combate donde eventos catastróficos cambian el devenir del juego.',
800, 'InfiniteCrisis.jpg');

INSERT INTO imagen_juego VALUES
(currval('videojuego_id_seq'), 'InfiniteCrisis.jpg');

INSERT INTO videojuego VALUES 
(DEFAULT, 1, 'Heroes and Generals',
to_tsvector('spanish','Heroes and Generals FPS multijugador online gratuito que cuenta con una campaña estratégica ambientada en plena II Guerra Mundial'),
'137OYoEd3eU',
'2014/06/19', 
'Heroes & Generals es un FPS multijugador online gratuito que cuenta con una campaña estratégica ambientada en plena II Guerra Mundial, cuando las potencias del Eje y los Aliados luchaban por el control en Europa.',
0, 'heroes-and-generals.jpg');

INSERT INTO imagen_juego VALUES
(currval('videojuego_id_seq'), 'heroes-and-generals.jpg');

INSERT INTO videojuego VALUES 
(DEFAULT, 1, 'World of Tanks',
to_tsvector('spanish','World of Tanks desarrollan batallas estratégicas a gran escala con carros de combate de la II Guerra Mundial'),
'iJXvmedxF-M',
'2012/04/12', 
'World of Tanks es uno de los juegos F2P (de acceso gratuito) más populares del mundo. En él se desarrollan batallas estratégicas a gran escala con carros de combate de la II Guerra Mundial y una mecánica de progresión adictiva. Y, para mayor profundidad e inmersión, la vista a través del cañón de un tanque de 1942 proporciona imágenes estereoscópicas producidas con la tecnología 3D Vision.

Fundada el año 1998 en Bielorrusia, Wargaming.net ha desarrollado y publicado numerosos títulos de estrategia para el mercado del PC durante más de una década apostando sobre seguro con juegos que sabía que gustarían a sus seguidores. Pero esto cambió en 2009, cuando la compañía decidió arriesgarlo todo con World of Tanks, un título multijugador F2P de combate con tanques que se movía entre el realismo más monótono y la emoción más trepidante.

Aunque inicialmente hizo poco ruido, atrajo mucha atención y el boca a boca hizo crecer rápidamente el número de usuarios del título, lo que le llevó a batir el récord Guiness del "servidor de MMO con mayor número de jugadores conectados al mismo tiempo" en 2011. Ahora, con más de veinte millones de usuarios registrados, World of Tanks es uno de los juegos online más populares del mercado y cuenta con más de medio millón de jugadores simultáneos al día.

Una de las claves de este crecimiento es la continua renovación de los contenidos con la introducción de tanques inéditos para nuevas naciones, además de otras muchas mejoras que incluyen la remodelación de los gráficos y la implementación de la tecnología NVIDIA 3D Vision.

Con porcentajes de beneficios mensuales de dos dígitos, Wargaming.net ha decidido reinvertir en dos títulos nuevos. El primero, World of Warplanes, ya está en fase beta y el segundo, World of Battleships, va camino de estrenarse en 2013. Con el tiempo, Wargaming.net espera integrar los tres títulos para que los aviones puedan bombardear objetivos estratégicos y afectar así a la situación de la naciones de World of Tanks o que los tanques puedan atacar puertos para retrasar la reparación de los destructores o desarbolar las defensas del contrario y facilitar ataques navales.',
0, 'WorldTanks.jpg');

INSERT INTO imagen_juego VALUES
(currval('videojuego_id_seq'), 'WorldTanks.jpg');

INSERT INTO videojuego VALUES 
(DEFAULT, 1, 'The Bureau: XCOM Declassified',
to_tsvector('spanish','The Bureau: XCOM Declassified narra el origen del primer encuentro de la organización clandestina XCOM con un enemigo misterioso y devastador'),
'NSkemNIF0Ag',
'2013/08/30', 
'La llegada de XCOM: Enemy Unknown, desarrollado por Firaxis Games, deslumbró a los amantes del género de estrategia por turnos y supuso todo un revulsivo para la saga. Ahora, The Bureau nos ofrece una nueva experiencia dentro del universo de XCOM.

Ambientado en 1962, en los momentos álgidos de la guerra fía, The Bureau narra el origen del primer encuentro de la organización clandestina XCOM con un enemigo misterioso y devastador. Constituida como una organización de defensa encubierta de los EE.UU. contra la Unión Soviética, The Bureau deberá adaptarse y enfrentarse a una amenaza que no se parece a nada de lo conocido hasta ahora. Encarnados en el agente especial William Carter, los jugadores deberán asumir el mando para dirigir a un grupo de agentes en una guerra secreta que decidirá el destino de la humanidad. Para neutralizar la amenaza, será fundamental la capacidad de The Bureau para ocultar la existencia del enemigo y evitar que se desencadene el pánico a nivel mundial.',
0, 'thebureau.jpg');

INSERT INTO imagen_juego VALUES
(currval('videojuego_id_seq'), 'thebureau.jpg');

INSERT INTO videojuego VALUES 
(DEFAULT, 1, 'AirMech',
to_tsvector('spanish','AirMech los últimos supervivientes de la Gran Guerra prosiguen su lucha ayudándose de la máquina de guerra definitiva: el AirMech.'),
'YXGFJKIP_PQ',
'2012/01/11', 
'En el futuro, los últimos supervivientes de la Gran Guerra prosiguen su lucha ayudándose de la máquina de guerra definitiva: el AirMech. el AirMech. Surca los aires en tu propio AirMech y juega de forma competitiva o cooperativa a este juego gratuito de acción y estrategia en tiempo real.

AirMech es un juego gratuito de acción del género RTS en el que los jugadores controlan robots metamórficos al estilo de Herzog Zwei y Defense of the Ancients. Cada robot exhibe sus propias habilidades especiales y cada piloto cuenta con sus propias estadísticas. A medida que avanza la acción, los jugadores adquieren reconocimiento y experiencia, lo que les permite desbloquear una amplia variedad de AirMechs, pilotos, unidades y objetos para personalizar su ejército como consideren oportuno.

Enfréntate a otros jugadores en escenarios de combate 1v1 y 3v3, cruza los cielos cooperando con ellos en batallas contra la IA e intenta sobrevivir a las interminables hordas de enemigos que se acercan en modo de supervivencia. Con controles fáciles de manejar totalmente compatibles con los mandos de la consola, AirMech constituye una novedad accesible y divertida en el panorama de los juegos de acceso gratuito.',
0, 'AirMech.jpg');

INSERT INTO imagen_juego VALUES
(currval('videojuego_id_seq'), 'AirMech.jpg');

INSERT INTO videojuego VALUES 
(DEFAULT, 1, 'Oil Rush',
to_tsvector('spanish','Oil Rush mundo posapocalíptico en el que la falta de dos bienes codiciados, petróleo y tiempo, ha provocado una lucha'),
'7Vo2qSyQ4S0',
'2012/01/25', 
'Nos encontramos en un mundo posapocalíptico en el que la falta de dos bienes codiciados, petróleo y tiempo, ha provocado una lucha sin cuartel entre la población. Los últimos supervivientes, en un intento desesperado de hacerse con el control y dominar al enemigo, han iniciado una contienda naval que ha convertido el planeta en un gigantesco campo de batalla. En estos tiempos duros y crueles, el petróleo une más que la sangre.

Participa en esta lucha feroz entre armadas enemigas a lo largo y ancho de mares sin fronteras. Tu misión será capturar plataformas petrolíferas, extraer el preciado oro negro y destruir al enemigo por mar y aire.

Oil Rush es un juego de estrategia en tiempo real basado en el control de grupos de unidades. Ofrece la mecánica de un RTS clásico combinada con aspectos típicos del género de torres de defensa. Esto significa que deberás controlar las plataformas de producción y sus torres de defensa, y enviar de unidades navales y aéreas a capturar las plataformas petrolíferas enemigas.',
0, 'OilRush.jpg');

INSERT INTO imagen_juego VALUES
(currval('videojuego_id_seq'), 'OilRush.jpg');

INSERT INTO videojuego VALUES 
(DEFAULT, 1, 'Anomaly 2',
to_tsvector('spanish','Anomaly 2 juego de estrategia en tiempo real que lleva el concepto de "ataque a la torre"'),
'Z-JqOKFaxoU',
'2015/04/14', 
'Anomaly 2 es un juego de estrategia en tiempo real que lleva el concepto de "ataque a la torre" desarrollado en Anomaly Warzone Earth a otro nivel. Aunque mantiene los principios fundamentales del original (planificación táctica y comandante sobre el terreno para apoyar a las tropas de combate), Anomaly 2 introduce una serie de novedades importantes.
Durante los años posteriores a la invasión de la Tierra en 2018, las máquinas alienígenas han ocupado el planeta. La especie humana está al borde de la extinción. Agrupados en enormes convoyes, los humanos recorren la tundra helada en busca de alimentos y provisiones. Desde la guerra, los papeles se han invertido: ahora nuestra especie parece ser la anomalía en un planeta controlado por máquinas. Tú eres el comandante, tu convoy se llama Yukon.',
0, 'Anomaly.jpg');

INSERT INTO imagen_juego VALUES
(currval('videojuego_id_seq'), 'Anomaly.jpg');

INSERT INTO videojuego VALUES 
(DEFAULT, 1, 'Titanfall',
to_tsvector('spanish','Titanfall es el primer shooter de nueva generación que combina adrenalina pura, carreras por muros y saltos dobles'),
'N1vqjJRcCzg',
'2014/03/13', 
'Titanfall es el primer shooter de nueva generación que combina adrenalina pura, carreras por muros y saltos dobles con la potencia y agilidad de los titanes en el combate para marcar un nuevo hito en el género multijugador online.',
670, 'Titalfall.jpg');

INSERT INTO imagen_juego VALUES
(currval('videojuego_id_seq'), 'Titalfall.jpg');

INSERT INTO videojuego VALUES 
(DEFAULT, 1, 'Strife',
to_tsvector('spanish','Strife juego colaborativo emocionante y cargado de acción para múltiples jugadores'),
'wxen_OUq-z8',
'2015/04/14', 
'Strife es un juego colaborativo emocionante y cargado de acción para múltiples jugadores. En partidas estratégicas que se desarrollan por sesiones, tienes que convertir en experto a un héroe principiante y a la vez aumentar tu poder invirtiendo en nuevos objetos y mascotas para él.

Strife es un juego de combate online multijugador (MOBA) en el que dos equipos de 5 miembros cada uno se enfrentan para destruir las torres, los generadores y, por último, la base del equipo contrario. En Strife el juego se desarrolla por sesiones. En cada partida, los jugadores convierten en experto al héroe principiante de su elección e incrementan su poder para que su equipo gane el juego. Con cada partida que juegan aumenta el poder de su cuenta, ya que obtienen recompensas para invertir en nuevos objetos y mascotas que el héroe utilizará en las siguientes partidas.

EN STRIFE PODRÁS:
Competir con los enemigos, no con los compañeros del equipo: el concepto colaborativo de este juego ha eliminado la lucha por los recursos (el oro se comparte, por lo que ya no hay peleas por el último golpe), los roles designados y los problemas con la experiencia de juego.

Experiencia visual atractiva y nítida: la intensidad y la agilidad del juego te absorberán hasta perder el sentido de la realidad, pero no a tu héroe en los combates de equipo. La nitidez de las imágenes de Strife permite a todos los participantes de un ataque seguir la secuencia de la batalla con facilidad.

Diviértete con el rol que elijas: ningún jugador tiene por qué sentirse relegado a desempeñar el papel sacrificado del tradicional héroe secundario de los MOBA. Todos los héroes del equipo pueden contribuir a la victoria y todos pueden reunir recursos y coordinar las aptitudes de su héroe con los demás miembros para lograr un efecto combinado devastador. El equilibrio y la mecánica de juego de Strife están diseñados para que todo el mundo pueda ser activo y luchar durante más tiempo.

Reinvéntate: el progreso personalizable te permite avanzar en los aspectos que elijas. Los objetos son adaptables a la medida de tus habilidades en el juego, mientras que con las mascotas alcanzarás un nivel más de personalización para complementar las aptitudes de los héroes. Strife te permite jugar a tu manera, innovar y dominar.

Sumérgete en todo un universo: Strife narra la historia del viaje personal de cada héroe y la causa que ha llevado a cada uno a entrelazar su trayectoria con las pruebas de Strife. El rico universo de Strife transporta al jugador a un contexto bien trabado donde las aportaciones de la comunidad son lo que decide la trama en última instancia.',
540, 'Strife.jpg');

INSERT INTO imagen_juego VALUES
(currval('videojuego_id_seq'), 'Strife.jpg');

INSERT INTO videojuego VALUES 
(DEFAULT, 1, 'League Of Legends',
to_tsvector('spanish','League Of Legends línea competitivo de ritmo frenético, que fusiona la velocidad y la intensidad de la estrategia en tiempo real (ETR)'),	
'j-hXQ1oEn84',
'2013/10/19', 
'League of Legends es un juego en línea competitivo de ritmo frenético, que fusiona la velocidad y la intensidad de la estrategia en tiempo real (ETR) con elementos de juegos de rol. Dos equipos de poderosos campeones, cada uno con un diseño y un estilo de juego únicos, compiten cara a cara a través de diversos campos de batalla y modos de juego. Con un plantel de campeones en constante expansión, actualizaciones frecuentes y un emocionante panorama competitivo, League of Legends ofrece posibilidades de juego ilimitadas para usuarios de todos los niveles de habilidad.

Combate cara a cara
Combina pensamiento estratégico, reflejos veloces y juego en equipo coordinado para aplastar a tus enemigos, tanto en pequeñas escaramuzas como en intensas batallas de 5v5.
Prepara tus estrategias y evoluciona
Con actualizaciones de juego regulares, varios mapas y modos de juego, y nuevos campeones que se unen a la Liga constantemente, el único límite posible que se le puede poner a tu éxito es tu propia ingenuidad.
Compite a tu manera
Tanto si quieres disfrutar jugando contra bots como si deseas subir posiciones en el sistema de liga, League of Legends tiene la tecnología necesaria para emparejarte con un grupo de competidores de nivel similar.
Lucha con honor
Compite con honor y recibe menciones especiales de los demás jugadores para recompensar tu forma de jugar.
Vive la emoción de los e-sports
League of Legends tiene la escena competitiva más activa del mundo, por lo que cuenta con numerosos torneos por todo el mundo, incluida la prestigiosa Championship Series en la que profesionales con sueldo compiten para llevarse millones.
La comunidad de juego en línea más grande del mundo
Únete a la comunidad de juego en línea más grande del mundo: haz amigos, forma equipos y enfréntate a decenas de millones de oponentes de países de todo el orbe para luego intercambiar opiniones en reddit, YouTube, foros y mucho más.',
0, 'LeagueOfLegends.jpg');

INSERT INTO imagen_juego VALUES
(currval('videojuego_id_seq'), 'LeagueOfLegends.jpg');