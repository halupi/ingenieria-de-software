var ControladorListado = function($scope, $state, $http) {
	$scope.juegos = [];

	if( !$scope.usuario_login.es_admin )
		$state.go("404");

	$scope.fetch = function() {
		$http.get("/juegos/recientes")
			.success(function(response) {
				$scope.juegos = response;
			});
	}

	$scope.fetch();
}