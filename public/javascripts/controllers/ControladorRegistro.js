var ControladorRegistro = function($scope, $http, Upload, $state, $stateParams) {
	$scope.usuario = { creditos: 0 };
	$scope.validar = false;
	$scope.archivos = [];

	$scope.$watch("password", function() {
		$scope.formulario.repetir_password.$setValidity('required', false);
	});

	$scope.$watch("archivos", function() {
		if( $scope.archivos instanceof Array && $scope.archivos.length > 0 )
			$scope.usuario.historial = $scope.archivos[0].name;
	});

	$scope.fetch = function() {
		$scope.edicion = true;
		$scope.usuario = {};

		$http.get("/registro/" + $stateParams.id ).success(function(response) {
			if( response.error )
				$.bigBox({
					title: "Error del Servidor",
					content: response.mensaje,
					color: "#A00000",
					icon: "fa fa-lock",
					timeout: 5000
				});
			else
				$scope.usuario = response;
		});
	}

	$scope.submit = function() {

		$scope.validar = true;

		if( $scope.formulario.$valid )
			async.series([
				function(callback) {
					if( $scope.archivos.length > 0 ) {
						Upload.upload({
							url: "/registro/archivo",
							file: $scope.archivos[0]
						}).success(function (data, status, headers, config) {
							if( data.error ) {
								$.bigBox({
									title: "Error del Servidor",
									content: data.mensaje,
									color: "#A00000",
									icon: "fa fa-file",
									timeout: 5000
								});
								return callback({});
							} else if( typeof data.archivo !== "undefined" ) {
								$scope.usuario.historial = data.archivo;
								return callback(null);
							}
						});
					} else 
						return callback(null);
				},

				function(callback) {
					var exito = $scope.edicion ? "Se han actualizado tus datos exitósamente" : "Su registro se ha completado exitósamente. El administrador le notificará la aprobación de su historial académico.";
					$http.post("/registro/guardar", $scope.usuario)
						.success(function(response) {
							if( response.error ) {
								$.bigBox({
									title: "Error del Servidor",
									content: response.mensaje,
									color: "#A00000",
									icon: "fa fa-times",
									timeout: 5000
								});

								return callback({});
							} else {
								$.bigBox({
									title: "Operación exitosa",
									content: exito,
									color: "#3c763d",
									icon: "fa fa-check",
									timeout: 5000
								});

								return callback(null);
							}
						});
				}
			], function(err) {
				if( !err ) {
					$scope.usuario.logged_in = true;
					var creditos = $scope.usuario_login.creditos || 0;
					$scope.$parent.usuario_login = $scope.usuario;
					$scope.$parent.usuario_login.creditos = creditos;
					$state.go("juegos", {filtro: "todos"});
				}
			});
		else
			$.bigBox({
				title: "Formulario Inválido",
				content: "Los campos marcados en rojo faltan o tienen valores inválidos.",
				color: "#A00000",
				icon: "fa fa-times",
				timeout: 5000
			})
	}

	if( typeof $stateParams.id !== "undefined" )
		$scope.fetch();
}