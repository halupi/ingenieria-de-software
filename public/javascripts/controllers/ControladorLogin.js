var ControladorLogin = function($scope, $http, $state) {
	$scope.login = {};

	$scope.submit = function() {
		$http.post("/login", $scope.login)
			.success(function(response) {
				if( response.error )
					$.bigBox({
						title: "No se pudo iniciar sesión",
						content: response.mensaje,
						color: "#A00000",
						icon: "fa fa-lock",
						timeout: 5000
					});
				else {
					$("body").removeClass('anim_boot');
					$scope.$parent.usuario_login = response;
					$scope.$parent.usuario_login.logged_in = true;
					$.bigBox({
						title: "¡Bienvenido " + $scope.$parent.usuario_login.nombre + "!",
						content: "Has iniciado sesión exitósamente.",
						color: "#3c763d",
						icon: "fa fa-check",
						timeout: 5000
					});
					
					$state.go( $scope.$parent.usuario_login.es_admin ? "admin.listado" : "juegos", {}, {reload: true} );
					$("#email, #password").val("");
				}
			});
	}
}