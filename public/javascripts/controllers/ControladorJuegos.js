var ControladorJuegos = function($scope, $http, $sce, $state, $stateParams, $rootScope, $location) {
	$scope.juegos = [];
	$scope.pagina = $stateParams.p ? parseInt($stateParams.p) : 1;
	$scope.titulo;
	$scope.paginador = [];
	$scope.total_paginas = 0;
	$scope.busqueda = false;

	function filtro_valido(filtro) {
		return filtro === "todos" || filtro === "recientes" || filtro === "vendidos" || filtro === "gratuitos";
	}

	function cambiar_titulo(filtro) {
		var titulo;

		switch(filtro) {
			case "vendidos":
				titulo = "Lo más vendidos";
				break;

			case "recientes":
				titulo = "Los más recientes";
				break;

			case "gratuitos":
				titulo = "Gratuitos"
				break;

			case "todos":
			default:
				titulo = "Todos los juegos";
		}

		$scope.titulo = titulo;
	}

	$scope.filtro = filtro_valido( $stateParams.filtro ) ? $stateParams.filtro : "todos";

	$scope.elegido = { imagenes: [] };

	$scope.fetch = function() {
		$http.get("/juegos/" + $scope.filtro + "?p=" + $scope.pagina)
			.success(function(response) {
				cambiar_titulo( $scope.filtro );
				$scope.juegos = response;

				if( $scope.juegos.length > 0 ) {
					$scope.total_paginas = Math.ceil( $scope.juegos[0].total / 10 );
					$scope.paginador = new Array( $scope.total_paginas );
				}

			});
	}

	$scope.paginar = function(pagina) {
		if( pagina == 0 || pagina == ($scope.total_paginas + 1) )
			return;
		$state.go("juegos", {filtro: $scope.filtro, p: pagina}, {location: "replace"});
	}

	$scope.seleccionarJuego = function(juego) {
		$scope.elegido = juego;

		setTimeout(function(){
			$('.materialboxed').materialbox();
		},1000);
	}

	$scope.sanitizar = function(src) {
		return $sce.trustAsResourceUrl(src);
	}

	$scope.comprar = function(juego) {
		$http.post("/juegos/comprar", { videojuego_id: juego.id }).success(function(response) {
			if( response.ok ) {
				$.bigBox({
					title: "Operación exitosa",
					content: response.mensaje,
					color: "#3c763d",
					icon: "fa fa-check",
					timeout: 5000
				});
				juego.comprado = true;
				$scope.usuario_login.creditos -= response.descontado;
			} else if( response.error )
				$.bigBox({
					title: "Error en la operación",
					content: response.mensaje,
					color: "#A00000",
					icon: "fa fa-times",
					timeout: 5000
				});
			else
				$.bigBox({
					title: "Error en la operación",
					content: "Ocurrió un error inesperado",
					color: "#A00000",
					icon: "fa fa-times",
					timeout: 5000
				});
		});
	}

	$rootScope.$on("busqueda", function(event, data) {
		$scope.titulo = "Resultados de Búsqueda de:  " + data.termino;
		$scope.es_busqueda = true;
		$scope.juegos = data;
	});

	$scope.$parent.juegos();
	$scope.fetch();
}