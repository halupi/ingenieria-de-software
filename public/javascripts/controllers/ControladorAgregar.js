var ControladorAgregar = function($scope, $http, Upload, $state, $stateParams) {
	$scope.juego = {};
	$scope.imagenes = [{}];
	$scope.archivos = [];
	$scope.validar = false;
	$scope.juego.imagenes_borradas = [];
	$scope.edicion = false;
	$scope.cargando = false;

	if( !$scope.usuario_login.es_admin )
		$state.go("404");

	$scope.submit = function() {
		$scope.validar = true;
		$scope.cargando = true;

		$scope.juego.imagenes = [];
		$scope.juego.archivo = undefined;

		if( $scope.formulario.$valid )
			async.series([
				function(next_series) {
					async.eachSeries($scope.imagenes, function(imagen, next_each) {
						console.log("Subiendo imagen...", imagen.file.name)
						Upload.upload({
							url: "/admin/agregar/archivo",
							file: imagen.file
						}).success( function(data) {
							console.log("Respuesta!", data, "\n")
							if( data.error ) {
								$.bigBox({
									title: "Error al subir la imagen " + imagen.name,
									content: "Ocurrió un error mientras se procesaba la imagen",
									icon: "fa fa-times",
									color: "#A00000",
									timeout: 5000
								});
								next_each({ error: true });
							} else  {
								$scope.juego.imagenes.push( imagen.file.name );
								next_each();
							}
						});
					}, function(err) {
							console.log("Terminado de subir imagenes", err);
							if( err )
								return next_series({ error: true });
							next_series();
					});
				},

				function(next_series) {
					Upload.upload({
						url: "/admin/agregar/juego",
						file: $scope.juego.file
					}).success( function(data) {
						if( data.error ) {
							$.bigBox({
								title: "Error al subir el archivo " + juego.file.name,
								content: "Ocurrió un error mientras se procesaba el archivo, intenta de nuevo.",
								icon: "fa fa-times",
								color: "#A00000",
								timeout: 5000
							});
							next_series({ error: true });
						} else {
							$scope.juego.archivo = $scope.juego.file.name;
							next_series();
						}
					});
				}
			], function(err) {
				if( !err ) {
					$http.post("/admin/agregar", $scope.juego)
						.success(function(response) {
							if( response.error )
								$.bigBox({
									title: "Error del Servidor",
									content: response.mensaje,
									icon: "fa fa-times",
									color: "#A00000",
									timeout: 5000
								});
							else {
								$.bigBox({
									title: "Operación exitosa",
									content: "Su juego se ha publicado con éxito.",
									color: "#3c763d",
									icon: "fa fa-check",
									timeout: 5000
								});

								$state.go("admin.listado");
							}
						});
				}
				$scope.cargando = false;
			});
		else
			$scope.cargando = false;
	}

	$scope.asociarImagen = function(archivos, index) {
		if( typeof archivos[0] !== "undefined" ) {
			$scope.imagenes[index].file = archivos[0];
			$scope.imagenes[index].name = archivos[0].name;
		}
	}

	$scope.$watch("archivos_juego", function() {
		if( $scope.archivos_juego instanceof Array && $scope.archivos_juego.length > 0 ) {
			$scope.juego.file = $scope.archivos_juego[0];
			$scope.juego.filename = $scope.juego.file.name;
		}
	});

	$scope.agregarImagen = function() {
		$scope.imagenes.push({});
	}

	$scope.eliminarImagen = function(index) {
		if( $scope.imagenes.length == 1 )
			$.bigBox({
				title: "Error al eliminar",
				content: "Debe haber por lo menos 1 imagen.",
				icon: "fa fa-times",
				color: "#A00000",
				timeout: 5000
			});
		else {
			var elemento = $scope.imagenes.splice(index, 1);
			console.log(elemento[0]);
			console.log("preparando para eliminar " + elemento.name);
			$scope.juego.imagenes_borradas.push(elemento[0].name);
		}
	}

	$scope.fetch = function() {
		$scope.edicion = true;
		$scope.usuario = {};

		$http.get("/admin/agregar/" + $stateParams.id ).success(function(response) {
			if( response.error )
				$.bigBox({
					title: "Error del Servidor",
					content: response.mensaje,
					color: "#A00000",
					icon: "fa fa-lock",
					timeout: 5000
				});
			else{
				$scope.juego = response;
				$scope.juego.imagenes_borradas = [];
				$scope.juego.file = new File([response["juego"]], 
							response["juego"], 
							{lastModified: Date.now()});
				$scope.juego.filename = response["juego"];
				$scope.imagenes = [];
				for(cont = 0; cont < response["imagenes"].length; cont++){
					console.log(response["imagenes"][cont]);
					$scope.imagenes.push({ 
						file:
						new File([response["imagenes"][cont]], 
							response["imagenes"][cont], 
							{lastModified: Date.now()}),
						name: response["imagenes"][cont]});
				}
			}
		});
	}

	if( typeof $stateParams.id !== "undefined" )
		$scope.fetch();
}