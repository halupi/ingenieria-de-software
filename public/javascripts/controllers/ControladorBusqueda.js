var ControladorBusqueda = function($scope, $http, $state, $rootScope) {
	$scope.busqueda = "";

	$scope.submit = function() {
		$state.go("juegos").then(function(){
			$http.get("/busqueda?q=" + $scope.busqueda)
				.success(function(response) {
					response.termino = $scope.busqueda;
					$rootScope.$emit("busqueda", response);
				});
		});
	}

	$scope.limpiar = function() {
		if( $scope.busqueda.trim().length > 0 ) {
			$scope.busqueda = "";
			$state.go("juegos", {filtro: "todos"}, {reload: true});
		}
	}
}