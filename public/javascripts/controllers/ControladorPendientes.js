var ControladorPendientes = function($scope,$http) {
	$scope.pendientes = [];
	$scope.pendiente = {};
	$scope.index = 0;
	$scope.validar = false;
	$scope.edicion = true;

	if( !$scope.usuario_login.es_admin )
		$state.go("404");

	$scope.fetch = function(){
		$http.get("/admin/pendientes").success(function(response){
				$scope.pendientes = response;
		});
	}

	$scope.seleccionar = function(pendiente, index) {
		$scope.pendiente = pendiente;
		$scope.index = index;
		$scope.valida = false;
	}

	$scope.hay_pendiente = function() {
		return Object.keys( $scope.pendiente ).length > 0;
	}
    
    $scope.aprueba = function(){
    	$scope.validar = true;
    	if( $scope.formulario.$valid ) {
    		$http.post("/admin/aprueba", $scope.pendiente )
				.success(function(response){
					if( response.error )
						$.bigBox({
							title: "Error al aprobar",
							content: response.mensaje,
							icon: "fa fa-times",
							color: "#A00000",
							timeout: 5000
						});
					else {
						$.bigBox({
							title: "Crédito Aprobado",
							content: "Se ha realizado la transacción con éxito.",
							icon: "fa fa-check",
							color: "#3c763d",
							timeout: 5000
						});
						$scope.pendiente = {};
						$scope.pendientes.splice($scope.index, 1);
					}
					$scope.fetch();
				});
    	} else
    		$.bigBox({
				title: "Formulario Inválido",
				content: "Los campos marcados en rojo faltan o tienen valores inválidos.",
				color: "#A00000",
				icon: "fa fa-times",
				timeout: 5000
			})
	}

	$scope.deniega = function(){	
		$http.post("/admin/denegar", $scope.pendiente)
			.success(function(response){
				if( response.error )
					$.bigBox({
						title: "Error al denegar",
						content: response.mensaje,
						icon: "fa fa-times",
						color: "#A00000",
						timeout: 5000
					});
				else {
					$.bigBox({
						title: "Crédito Denegado",
						content: "Se ha realizado la transacción con éxito.",
						icon: "fa fa-check",
						color: "#3c763d",
						timeout: 5000
					});
					$scope.pendiente = {};
					$scope.pendientes.splice($scope.index, 1);
				}
				$scope.fetch();
			});
		}
	$scope.fetch();
}	
