var ControladorApp = function($scope, $http, $state, $rootScope) {
	$scope.usuario_login = { logged_in: false, id: 0 };

	$scope.fetch = function() {
		$http.get("/sesion").success(function(response) {
			if( response.id ) {
				$scope.usuario_login = response;
				$scope.juegos();
			}
		});
	}

	$scope.juegos = function() {
		$http.get("/comprados").success(function(response) {
			$scope.usuario_login.juegos = response;
		});
	}

	$scope.comprado = function(juego) {
		if( !$scope.usuario_login.juegos instanceof Array )
			return false;
		for(var index in $scope.usuario_login.juegos ) {
			var _juego = $scope.usuario_login.juegos[index];

			if( _juego.id === juego.id )
				return true;
		}

		return false;
	}

	$scope.logout = function() {
		$http.get("/logout").success(function(response) {
			$scope.usuario_login = { logged_in: false, id: 0 };
			$state.go("juegos", {}, {reload: true});
		});
	}

	$scope.error = function() {
		$state.go("404");
	}

	$rootScope.$on("$stateChangeStart", function() {
		$(".loader").fadeIn();
	});

	$rootScope.$on("$viewContentLoaded", function() {
		setTimeout(function() {
			$(".loader").fadeOut("slow");
		}, 1000);
	});

	$scope.fetch();
}