var app = angular.module("VGStoreApp", ["ui.router", "ngFileUpload", "ngSanitize"]);

app.config(function($stateProvider, $urlRouterProvider) {
	$urlRouterProvider.otherwise("/juegos/todos");

	$stateProvider
		.state('404', {
			url: "/404",
			templateUrl: "/parcial/404"
		})
		.state('registro',{
			url: "/registro",
			templateUrl: "/parcial/registro",
			controller: "ControladorRegistro"
		})
		.state('actualizar', {
			url: "/registro/:id",
			templateUrl: "/parcial/actualizar",
			controller: "ControladorRegistro"
		})
		.state('juegos', {
			url: "/juegos/:filtro?p",
			templateUrl: "/parcial/juegos",
			controller: "ControladorJuegos"
		})
		.state('cuenta', {
			url: "/cuenta",
			templateUrl: "/parcial/cuenta",
			controller: "ControladorCuenta"
		})
		.state('admin', {
			abstract: true,
			url: "/admin",
			templateUrl: "/parcial/admin"
		})
		.state('admin.listado', {
			url: "/juegos",
			templateUrl: "/parcial/admin/juegos",
			controller: "ControladorListado"
		})
		.state('admin.agregar', {
			url: "/agregar",
			templateUrl: "/parcial/admin/agregar",
			controller: "ControladorAgregar"
		})
		.state('admin.editar', {
			url: "/agregar/:id",
			templateUrl: "/parcial/admin/agregar",
			controller: "ControladorAgregar"
		})
		.state('admin.pendientes', {
			url: "/pendientes",
			templateUrl: "/parcial/pendientes",
			controller: "ControladorPendientes"
		});
});

app.controller("ControladorApp", ["$scope", "$http", "$state", "$rootScope", ControladorApp]);
app.controller("ControladorLogin", ["$scope", "$http", "$state", ControladorLogin]);
app.controller("ControladorRegistro", ["$scope", "$http", "Upload", "$state", "$stateParams", ControladorRegistro]);
app.controller("ControladorJuegos", ["$scope", "$http", "$sce", "$state", "$stateParams", "$rootScope", "$location", ControladorJuegos]);
app.controller("ControladorAgregar", ["$scope", "$http", "Upload", "$state", "$stateParams",ControladorAgregar]);
app.controller("ControladorListado", ["$scope", "$state", "$http", ControladorListado]);
app.controller("ControladorBusqueda", ["$scope", "$http", "$state", "$rootScope", ControladorBusqueda]);
app.controller("ControladorPendientes", ["$scope", "$http", "$state", ControladorPendientes]);
app.controller("ControladorCuenta", ["$scope", "$http", ControladorCuenta]);
