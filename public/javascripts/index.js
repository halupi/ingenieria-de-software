var cntLog = $("#cont_login");
$(".login").on("click", function(){
	if($("body").hasClass("anim_boot")){
		$("body").removeClass('anim_boot');
	}else{
		if(this.getAttribute("dev") == "desk") {
			$("body").addClass("anim_boot");
		}else{
			$('.drag-target').click();
			setTimeout(function(){
				$("body").addClass("anim_boot");
			}, 400);
		}
	}
});
cntLog.on('mouseleave',function(){
	$('body.anim_boot').on("click",function(){
		$("body").removeClass('anim_boot');
		$("body").off("click");
		$("#email, #password").val("");
	});
});
cntLog.on('mouseenter',function(){
	$("body").off("click");
});

$(document).ready(function() {
	if($("#particle").length != 0){
		$('#particle').particleground({
		  minSpeedX: 0.1,
		  maxSpeedX: 1,
		  minSpeedY: 0.1,
		  maxSpeedY: 1,
		  directionX: 'center', // 'center', 'left' or 'right'. 'center' = dots bounce off edges
		  directionY: 'center', // 'center', 'up' or 'down'. 'center' = dots bounce off edges
		  density: 18000, // How many particles will be generated: one particle every n pixels
		  dotColor: '#4db6ac',
		  lineColor: '#cccccc',
		  particleRadius: 5, // Dot size
		  lineWidth: 1,
		  curvedLines: false,
		  proximity: 175, // How close two dots need to be before they join
		  parallax: false,
		  parallaxMultiplier: 15, // The lower the number, the more extreme the parallax effect
		  onInit: function() {},
		  onDestroy: function() {}
		});
	}
	$(".button-collapse").sideNav();
});



