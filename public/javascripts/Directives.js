app.directive("ngEquals", function() {
	return {
		scope: true,
		require: "ngModel",
		link: function($scope, $elem, $attrs, $ctrl) {
			$ctrl.$parsers.unshift(function(value) {
				$ctrl.$setValidity("ngEquals", $attrs.ngEquals === value);
				$ctrl.$setValidity("required", true);
				return value;
			});

			$ctrl.$formatters.unshift(function(value) {
				$ctrl.$setValidity("ngEquals", $attrs.ngEquals === value);
				$ctrl.$setValidity("required", true);
				return value;
			});
		}
	}
});

app.directive('initModal', function() {
	return function(scope, element, attrs) {
		if (scope.$last)
			$('.modal-trigger').leanModal();
			$(".waves-effect.waves-light.btn.green.darken-2.modal-trigger").on("click", function(){
				$("#modal1").scrollTop(0);
			})
	};
});

app.directive("ngPrefill", function() {
	return {
		scope: true,
		require: 'ngModel',
		link: function(scope, element, attrs, ngModel) {
			if( scope.edicion )
				element.next().addClass("active");
		}
	}
});

app.directive('soloNumeros', function(){
   return {
     require: 'ngModel',
     link: function(scope, element, attrs, modelCtrl) {
       modelCtrl.$parsers.push(function (inputValue) {
           if (inputValue == undefined) return '' 
           var transformedInput = inputValue.replace(/[^0-9]/g, ''); 
           if (transformedInput!=inputValue) {
              modelCtrl.$setViewValue(transformedInput);
              modelCtrl.$render();
           }         

           return transformedInput;         
       });
     }
   };
});

app.directive('soloFlotantes', function () {
	return {
		require: 'ngModel',
		link: function (scope) {	
			scope.$watch('usuario.promedio', function(newValue,oldValue) {
                var arr = String(newValue).split("");
                if (arr.length === 0) return;
                if (arr.length === 1 && (arr[0] == '-' || arr[0] === '.' )) return;
                if (arr.length === 2 && newValue === '-.') return;
                if (isNaN(newValue) && newValue !== undefined) {
                    scope.usuario.promedio = oldValue;
                }
            });
		}
	};
});

app.directive('soloLetras', function () {
    return {
        restrict: 'A',
        require: '?ngModel',
        link: function (scope, element, attrs, ngModel) {
            if (!ngModel) {
                return;
            }
            ngModel.$parsers.unshift(function (inputValue) {
                var alphabets = inputValue.split('').filter(function (s) {
                    return (isALetter(s));
                }).join('');
                ngModel.$viewValue = alphabets;
                ngModel.$render();
                return alphabets;
            });

            function isALetter(charVal)
		    {
		        if( charVal == " " || charVal.toUpperCase() != charVal.toLowerCase() ) {
		            return true;
		        }
		        else {
		            return false;
		        }
		    }
        }
    };
});